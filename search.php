<?php
/**
 * Template Name: Search
 *
 */
require_once('header.php'); 

global $paged, $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

array_merge( $search_query, $wp->query_vars );

$has_post_type = false;
$posts_per_page = 10;
$vars['posts_per_page'] = 10000;
$vars['paged']			= $paged;
$qvars = get_query_var('post_type');
$type = is_array($qvars) ? $qvars[0] : $qvars;
if($type && $type !='any'):
	$vars['post_type']	= $type;
	$vars['posts_per_page'] = $posts_per_page;
	$has_post_type = true;
endif;

$new_query = array_merge( $vars, $wp->query_vars );
query_posts($new_query);
?>
<div id="page-content" class="section hot-links">
	
		<h2 class="title"><?php printf( "Search results for '%s'", get_search_query() ); ?></h2>
		<div id="blog" <?php post_class('page'); ?>>
		<?php if ( have_posts() ) : ?>
			<?php get_search_form(); ?>
			<?php get_template_part( 'loop','all' ); ?>
		<?php else : ?>
				<p><?php printf( 'Sorry, your search for "%s" did not turn up any results. Please try again.', get_search_query());?></p>
				<?php get_search_form(); ?>
		<?php endif; ?>
	</div>
</div>
<div id="pagination">
<?php if(is_single()): ?>
	<span class="nav-new">
		<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
	</span>
	<span class="nav-old">
		<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
	</span>
<?php else: ?>
	<span class="nav-old">
		<?php next_posts_link('&larr; Older entries '); ?>
	</span>
	<span class="nav-new">
		<?php previous_posts_link('Newer entries &rarr;'); ?>
	</span>
<?php endif; ?>
</div><!-- /#pagination-->
<?php get_footer(); ?>
