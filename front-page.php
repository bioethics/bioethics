<?php
global $nav_editions;
require_once('header.php');
?>
<div id="home">
	<div class="section" id="blog">
		<div class="feature">
			<h2 class="rss"><a href="<?php bloginfo('rss2_url'); ?>"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Blog RSS"></a> <a href="<?php bloginfo('url')?>/blog">Blog.</a></h2>
			<div class="col col1">
				<?php
// feature posts w/o main feature
					$f = get_field('featured_blog_entry');
				$feature_id = $f->ID;
				$feature_description = get_field('description');
				query_posts('p=' . $feature_id);
				while (have_posts()) : the_post();
					?>
					<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
					<h5 class="date"><?php the_date(); ?></h5>
					<div class="description">
						<?php the_advanced_excerpt('exclude_tags=img'); ?>
					</div>
					<!-- /.description -->


				</div>
				<!-- /.col -->
				<div class="col col2">
					<div class="excerpt">
            <?php 
            if ( has_post_thumbnail() ) {
              echo "<a href=\"". get_permalink() ."\" title=\"". get_the_title() ."\">";
              the_post_thumbnail('home_blog_featured');
              echo "</a>";
            }
            ?>
						<?php // the_advanced_excerpt('exclude_tags=img'); ?>
						<p class="more"><a href="<?php the_permalink() ?>">Full Article</a></p>
						<?php edit_post_link('Edit this', '<p class="edit-link">', '</p>'); ?>
					</div>
					<!-- /.excerpt -->
				</div>
				<?php
			endwhile;
			wp_reset_query();
			?>  
			<!-- /.col -->
		</div>
		<!-- /.feature -->

		<div class="list">
			<h4>Other Featured Blog Articles</h4>
			<?php
// feature posts w/o main feature
      $itemscount = get_field('blog_items_count', get_the_ID());
			$args = array(
				'cat' => '29',
				'post__not_in' => array($feature_id),
				'posts_per_page' => $itemscount
			);
			$p = query_posts($args);
			while (have_posts()) : the_post();
				?>
				<div class="feature-item">
					<h2 class="date"><?php echo get_the_date()?></h2>
					<h4 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
				</div>
				<!-- /.feature-item -->

				<?php
			endwhile;
			wp_reset_query();
			?>
		</div>
		<!-- /.list -->
	</div>
	<!-- /#blog.section -->

	<div id="journal-feature" class="section">
		<div id="journal-detail">


			<?php
			$i = 1;
			$c = count($nav_editions);
			foreach ($nav_editions as $edition):
				?>

				<div data-id="<?php echo($i - 1) ?>" class="journal-item slide" style="display:none;">
					<div class="journal-detail">
						<a href="<?= get_permalink($edition->ID); ?>"><img src="<?= $edition->meta['cover_image']['sizes']['journal_medium'];?>" alt="No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?>"></a>
						<h3><?= $edition->edition->name ?></h3>
						<h4>VOL. <?= $edition->meta['volume'] ?> No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?></h4>
						<ul class="issues">
							<li><a href="<?= get_permalink($edition->ID); ?>" class="current">Current Issue</a></li>
							<li><a href="<?= get_bloginfo('url') . '/editions/' . $edition->edition->slug ?>" class="past">Past Issues</a></li>
							<!-- <li><a href="/about/editors/" class="editors">The Editors</a></li> -->
						</ul>
						<!-- /.issues -->
					</div>
					<!-- /.issue-detail -->
					<div class="journal-article">
						
						<?php
						if(get_field('target_articles',$edition->ID)): 
							$target = get_field('target_articles',$edition->ID);
							$target = $target[0];
							$author = get_field('primary_author',$target->ID);
							$page = get_field('page_number', $target->ID);
							$pg = explode("-", $page, 2);
							$firstpage = $pg[0];
							?>


						
						
						<h3><a href="<?= get_permalink($edition->ID); ?>"><?= $target->post_title; ?></a></h3>
						<h5 class="author">Written by <?= $author ?></h5>
						<div class="excerpt">
							<p><?php echo substr(strip_tags(get_field('abstract',$target->ID)),0,150);?>...</p>
<!--							<p class="more"><a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$edition->meta['isbn']?>&volume=<?=$edition->meta['volume']?>&issue=<?=$edition->meta['number'];?>&spage=<?=$firstpage?>" target="_blank">Full Article</a></p>							-->
						</div>
						<!-- /.excerpt -->		
						
						
						
						<?php 	endif; ?>
							
					</div>
					<!-- /.journal-article -->
				</div>
				<!-- /.journal-item -->

				<?php
				$i++;
			endforeach;
			?>
			<div id="journal-list">
				<?php
				$i = 1;
				foreach ($nav_editions as $edition):
					?>


					<div data-id="<?php echo($i - 1) ?>" class="journal-item-link slide-link" id="j-item<?= $i ?>">
	<img src="<?= $edition->meta['cover_image']['sizes']['journal_small'];?>" alt="VOL. <?= $edition->meta['volume'] ?> No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?>">
						<h3><?= $edition->edition->name ?></h3>
						<h4>VOL. <?= $edition->meta['volume'] ?> No.  <?= $edition->meta['number'] ?> |  <?= date('F Y', strtotime($edition->meta['publish_date'])); ?></h4>
					</div>
					<!-- /.journal-item-link -->

					<?php
					$i++;
				endforeach;
				?>
			</div>
			<!-- /#journal-list -->
		</div>
		<!-- /#journal-detail -->
	</div>
	<!-- /#journal-feature.section -->


	<div id="news" class="section">

		<h2 class="rss"><a href="/news/feed" title="Bioethics News RSS Feed"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Btn Rss"></a> Bioethics News.</h2>

		<div class="col list">


			<?php
			$news_total = get_field('news_items_count', get_the_ID());
// feature posts w/o main feature
			$args = array(
				'post_type' => 'news',
				'posts_per_page' => $news_total
			);
			query_posts($args);
			$i = 1;
			while (have_posts()) : the_post();
				?>


				<div class="feature-item">
					<h5 class="date"><?= get_the_date() ?></h5>
					<h6 class="title"><a href="<?php the_field('url')?>" target="_blank"><?php the_title() ?></a> <span><?php the_field('source') ?></span></h6>
					<div class="excerpt">
						<p><?php the_field('excerpt') ?></p>
					</div>
				</div>      
				<!-- /.feature-item -->


	<?php if ($i == ceil($news_total / 2)): ?>   
		</div>
		<!-- /.col -->
		<div class="col list">
				<?php
				endif;


				$i++;
			endwhile;
			wp_reset_query();
			?>

		</div>
		<!-- /.col -->
		<!-- #hot-topics.col -->
		<div class="col split" id="hot-topics">
			<h2 class="rss">Hot Topics.</h2>
			<ul>
<?php wp_list_categories('orderby=name&show_count=0&title_li=&echo=false&exclude=210,29'); ?>
			</ul>
		</div>
		<!-- /#hot-topics.col -->
	</div>
	<!-- /#news.section -->

</div>
<!-- /#home -->

<?php require_once('footer.php'); ?>
