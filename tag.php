<?php require_once('header.php'); 

global $paged;
$has_post_type = false;

$vars['posts_per_page'] = 10000;
$vars['paged']			= $paged;
if($_GET['post_type']):
	$vars['post_type']	= $_GET['post_type'];
	$vars['posts_per_page'] = $posts_per_page;
	$has_post_type = true;
endif;

$new_query = array_merge( $vars, $wp->query_vars );
query_posts($new_query);
?>

<div id="main-content" class="hot-links">
<?php if ( have_posts() ) : ?>
	<h2 class="rss">
		Tag: <span> <?php
			single_tag_title("");
		?></span>
	</h2>
	
	<?php	get_template_part( 'loop','all' ); ?>

<div id="pagination">
<?php if(is_single()): ?>
	<span class="nav-new">
		<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
	</span>
	<span class="nav-old">
		<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
	</span>
<?php else: ?>
	<span class="nav-old">
		<?php next_posts_link('&larr; Older entries '); ?>
	</span>
	<span class="nav-new">
		<?php previous_posts_link('Newer entries &rarr;'); ?>
	</span>
	<?php endif; ?>
</div><!-- /#pagination-->


	<?php else : ?>
	<h1>No Posts</h1>	
<?php endif; ?>
</div> <!-- /#main-content -->
<div id="sidebar-resources" class="sidebar section">
		<ul>
			<?php dynamic_sidebar('tag-sidebar'); ?>
		</ul>
</div>

<?php require_once('footer.php'); 