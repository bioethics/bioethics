<?php
$template_base = get_template_directory_uri();
$nav_editions = get_new_journals();
?><!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes('html') ?>> <!--<![endif]-->
	<head>
		<title><?php
/*
 * Print the <title> tag based on what is being viewed.
 */
global $page, $paged;

if($post_type=='issues'):
	$fields = get_fields();
	$tax = get_the_terms(get_the_ID(),'editions');
	foreach($tax as $t):
		echo $t->name." : Volume ".$fields['volume']." &bull;  Issue ". $fields['number']." | ";
	endforeach;
else:
	wp_title('|', true, 'right');
endif;
// Add the blog name.
bloginfo('name');

// Add the blog description for the home/front page.
$site_description = get_bloginfo('description', 'display');
if ($site_description && ( is_home() || is_front_page() ))
	echo " | $site_description";

// Add a page number if necessary:
if ($paged >= 2 || $page >= 2)
	echo ' | ' . sprintf('Page %s', max($paged, $page));
?></title>
		<link id="media-url" href="<?= $template_base ?>" rel="media-url">
		<meta charset="<?php bloginfo('charset'); ?>" />
		<meta name="viewport" content="width=1080,initial-scale=.65">
		<link rel="alternate" type="application/rss+xml" title="Bioethics.net: Blog Feed" href="<?php bloginfo_rss('rss2_url'); ?>" />
		<link rel="alternate" type="application/rss+xml" title="Bioethics.net: News Feed" href="<?php bloginfo_rss('rss2_url'); ?>?post_type=news" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<div id="global-wrap">
			<div id="header">
				<div class="container">
					<div id="ad">
						<!-- Ad Butler banner -->
						<script type="text/javascript">
						var rnd = window.rnd || Math.floor(Math.random()*10e6); var pid156745 = window.pid156745 || rnd; var plc156745 = window.plc156745 || 0; var abkw = window.abkw || ''; var absrc = 'http://ab164087.adbutler-axion.com/adserve/;ID=164087;size=728x90;setID=156745;type=js;kw='+abkw+';pid='+pid156745+';place='+(plc156745++)+';rnd='+rnd+'';
						document.write('<scr'+'ipt src="'+absrc+'" type="text/javascript"></scr'+'ipt>');
						</script>
						<a href="http://www.tandfonline.com/action/pricing?journalCode=uajb20" target="_blank" title="Subscribe to The American Journal of Bioethics"><img src="<?php echo get_template_directory_uri(); ?>/images/subscribe-button-top.png" width="251" height="90" alt="Subscribe to The American Journal of Bioethics" /></a>
					</div>
					<h1>
						<a href="<?php echo home_url(); ?>" class="ir" title="Bioethics.net - Where the World Finds Bioethics">Bioethics.net</a>
					</h1>
					<h2>
						<a href="<?php echo home_url(); ?>" class="ir" title="Bioethics.net - Where the World Finds Bioethics">Where the World Finds Bioethics</a>
					</h2>
					<div class="search-box">
						<?php get_search_form(); ?>
					</div>
					<?php
					wp_nav_menu(array(
						'theme_location' => 'primary_nav',
						'container' => 'div',
						'container_id' => 'primary-nav',
					));
					?>
					<div id="nav-journals" style="display: none;">
						<div class="close"><a href="">Close</a></div>
						<ul>
							<?php
							$i = 1;
							$c = count($nav_editions);
							foreach ($nav_editions as $edition):
								?>

								<li id="link-<?= $edition->edition->slug ?>">
									<div class="journal-detail<?php if ($i == $c) echo " last" ?>">
										<a href="<?= get_permalink($edition->ID); ?>"><img src="<?= $edition->meta['cover_image']['sizes']['thumbnail']; ?>" alt="VOL. <?= $edition->meta['volume']; ?> No. <?= $edition->meta['number']; ?> | <?= date('F Y', strtotime($edition->meta['publish_date'])); ?>"></a>
										<h3><?= $edition->edition->name; ?></h3>
										<h4>VOL. <?= $edition->meta['volume']; ?> No. <?= $edition->meta['number']; ?> | <?= date('F Y', strtotime($edition->meta['publish_date'])); ?></h4>
										<ul class="issues">
											<li><a href="<?= get_permalink($edition->ID); ?>" class="current">Current Issue</a></li>
											<li><a href="<?= get_bloginfo('url') . '/editions/' . $edition->edition->slug ?>" class="past">Past Issues</a></li>
											<!-- <li><a href="/about/editors/" class="editors">The Editors</a></li> -->
										</ul>
										<!-- /.issues -->
									</div>
								</li>

								<?php
								$i++;
							endforeach;
							?>
						</ul>
					</div> <!-- /#nav-journals -->
					<div id="secondary-nav">
						<div class="news-updated"> 
							<?php
							$latest_post = get_transient( 'latest_post' );
							if(false === $latest_post):
								$args = array(
									'post_type' => 'any',
									'fields' 		=> 'ids',
									'posts_per_page' => 1
								);
								$get_latest_post = new WP_Query($args);
								if ( $get_latest_post->have_posts() ) :
									while ( $get_latest_post->have_posts() ) : $get_latest_post->the_post();
										$latest_post = get_the_time(__('D M j | g:i A T', 'bioethics'));
										set_transient( 'latest_post', $latest_post, YEAR_IN_SECONDS );
										wp_reset_query();
									endwhile;
								endif;
							endif;
							echo $latest_post;
							?></div>
						<div id="secondary-links">
							<?php
							wp_nav_menu(array(
								'theme_location' => 'secondary_nav',
								'container' => 'div',
								'container_class' => 'nav',
							));
							?>
						</div>
						<div id="nav-social">follow us: 
							<a href="/about/rss-feeds" id="social-rss" class="ir">RSS</a> 
							<a href="https://www.facebook.com/groups/bioethics.net/" id="social-fb" class="ir" title="Bioethics.net on Facebook">Facebook</a> 
							<a href="http://twitter.com/bioethics_net" title="Bioethics.net on Twitter" id="social-twitter" class="ir"></a>
							<a href="http://www.linkedin.com/groups/American-Journal-Bioethics-Readers-Contributors-1036327" title="Bioethics.net on LinkedIn" id="social-linkedin" class="ir"></a>
						</div>
					</div> <!-- /#secondary-nav -->
				</div> <!-- /.container -->
			</div> <!-- /#header -->




			<div id="wrapper">
				<div class="content">
					<div class="container">