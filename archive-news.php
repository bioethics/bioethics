<?php require_once('header.php'); ?>
<div id="list-content">
	<?php 
	if ( have_posts() ) : ?>
		<h2 class="title"><a href="<?php bloginfo('rss2_url'); ?>?post_type=news"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Btn Rss"></a> Bioethics News.</h2>
		<?php get_template_part( 'loop','short' ); ?>
	<?php else : ?>
		<p>No posts found.</p>
	<?php endif; 
?>
</div><!--end content-->
<?php get_footer(); ?>
