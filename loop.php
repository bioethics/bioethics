<?php if (have_posts()) : ?>
	<?php 
	$syndicated_blog_meta = null;
	while (have_posts()) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php 
			$syndicated_class = (has_tag('syndicated')) ? 'syndicated_post' : 'original_post';
				post_class('post '.$syndicated_class); ?>>
			<div class="post-header">
				<!--<?php if($syndicated_class == 'original_post') echo "<h5 class='original-callout'>Featured Post</h5>"	?>-->
				<h5 class="date"><?php echo  get_the_date(); ?></h5>
				<h3 class="post-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php the_title(); ?>
					</a>
				</h3>
				<?php
				// If this is a syndicated post
				if(has_tag('syndicated')): ?>
				<div class="syndicated_byline">
					<?php
					$s_meta = get_metadata('post',get_the_ID());

					$feed_id = $s_meta['syndication_feed_id'][0];
					if(!isset($syndicated_meta[$feed_id]))
						$syndicated_blog_meta[$feed_id] = get_bookmark($feed_id);
					$current_blog_meta = $syndicated_blog_meta[$feed_id];

					?>
					<a href="<?php echo $s_meta['syndication_source_uri'][0];?>" target="_blank" title="<?=$current_blog_meta->link_description;?>"><?=$s_meta['syndication_source'][0];?></a>
				</div>
				<?php
				endif;?>
			</div><!--end post-header-->
			<div class="entry">
				<?php
				if (is_single()):
					the_content();
				else:
					the_advanced_excerpt('length=100'); 
				?>
				<p class="more"><a href="<?php the_permalink(); ?>" title="Permanent link to <?php the_title(); ?>">Full Article</a></p>
				<?php
				endif;
$categories_list = get_the_category_list( ', ');
$tag_list = get_the_tag_list( '', ', ');
?>
				<div class="meta">
<?php
				// If this is a syndicated post
				if(has_tag('syndicated') && is_single()): ?>
				<div class="syndicated_meta">
					<?php
					$s_meta = get_metadata('post',get_the_ID());

					$feed_id = $s_meta['syndication_feed_id'][0];
					if(!isset($syndicated_meta[$feed_id]))
						$syndicated_blog_meta[$feed_id] = get_bookmark($feed_id);
					$current_blog_meta = $syndicated_blog_meta[$feed_id];

					?>
					<p><span>Syndicated from:</span> <a href="<?php echo $s_meta['syndication_source_uri'][0];?>" target="_blank" title="<?=$current_blog_meta->link_description;?>"><?=$s_meta['syndication_source'][0];?></a><br>
						<a target="_blank" href="<?=$s_meta['syndication_permalink'][0];?>" title="<?=$s_meta['syndication_source'][0];?>: <?php the_title(); ?>">View original article</a></p>
				</div>
				<?php
				endif;
				echo "<p>";
				if ( '' != $tag_list ) {
					$utility_text = 'This entry was posted in %1$s and tagged %2$s.  Posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				} elseif ( '' != $categories_list ) {
					$utility_text = 'This entry was posted in %1$s. Posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				} else {
					$utility_text = 'This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				}

				printf(
					$utility_text,
					$categories_list,
					$tag_list,
					esc_url( get_permalink() ),
					the_title_attribute( 'echo=0' ),
					get_the_author(),
					esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
				);
				edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>
					</p>
				</div>
				<div class="social">
					<?php if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
				</div>


			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; ?>
	
	<div id="pagination">
	<?php if(is_single()): ?>
		<span class="nav-new">
			<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
		</span>
		<span class="nav-old">
			<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
		</span>
	<?php else: ?>
		<span class="nav-old">
			<?php next_posts_link('&larr; Older entries '); ?>
		</span>
		<span class="nav-new">
			<?php previous_posts_link('Newer entries &rarr;'); ?>
		</span>
	<?php endif; ?>
	</div><!-- /#pagination-->
<?php else : ?>
	<h1>No Posts!</h1>	
<?php endif; ?>
