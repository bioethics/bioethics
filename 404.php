<?php require_once('header.php'); ?>
	<h1 class="page-title">
		<?php _e( '404: Page Not Found', 'bioethics' ); ?>
	</h1>
	<div>
		<p><?php _e( 'We are terribly sorry, but the URL you typed no longer exists. It might have been moved or deleted. Try searching the site:', 'bioethics' ); ?></p>
		<?php get_search_form(); ?>
	</div>
<?php require_once('footer.php'); ?>