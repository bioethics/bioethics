<?php require_once('header.php'); ?>

<div id="main-content" class="section">
<?php if ( have_posts() ) : ?>
	  <h2 class="rss"><a href="<?php bloginfo('rss2_url'); ?>"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Blog RSS"></a> <a href="<?php bloginfo('url')?>/blog">Blog.</a></h2>
	<h2 class="rss">
			<?php if ( is_day() ) : ?>
				<?php printf( 'Daily Archives: %s', '<span>' . get_the_date() . '</span>' ); ?>
			<?php elseif ( is_month() ) : ?>
				<?php printf( 'Monthly Archives: %s', '<span>' . get_the_date(  'F Y', 'monthly archives date format' ) . '</span>' ); ?>
			<?php elseif ( is_year() ) : ?>
				<?php printf(  'Yearly Archives: %s', '<span>' . get_the_date( 'Y', 'yearly archives date format' ) . '</span>' ); ?>
			<?php elseif( is_category()): ?>
				Topic Archives: <span><?= single_cat_title( '', false ) ?></span>
			<?php elseif( is_tag): ?>
				Tag Archives: <span><?= single_tag_title( '', false ) ?></span>
			<?php else : ?>
				Blog Archives
			<?php endif; ?>
	</h2>
	<?php
	get_template_part( 'loop' );

	else : ?>
	<h1>No Posts</h1>	

		<?php endif; ?>
</div> <!-- /#main-content -->
<div id="sidebar-blog" class="sidebar section">
	<ul>
		<?php dynamic_sidebar('blog-sidebar'); ?>
	</ul>
</div>

<?php require_once('footer.php'); 