<?php
/**
 * Template Name: Newsletter Subscribe
 *
 */
require_once('header.php');
?>
<div id="page-content" class="section section about-content">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<h2 class="title"><?php the_title(); ?>.</h2>
			<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
				<div class="entry">
					<?php the_content(); ?>
					<!-- Form -->

					<form action="http://campaigns.diamondmerckenshogan.com/t/r/s/guhtlj/" method="post" id="subForm">
						<div>
							<span class="label"><label for="name">Name:</label></span>
							<span><input type="text" name="cm-name" id="name" size="25" /></span>
						</div>
						<div>
							<span class="label"><label for="guhtlj-guhtlj">Email Address:</label></span>
							<span><input type="text" name="cm-guhtlj-guhtlj" id="guhtlj-guhtlj" size="25" /></span>
						</div>
						<div>

							<div>
								<span class="label"><label for="Country">Country:</label></span>
								<span><select name="cm-fo-ajyjyh">
											<option value="2818626">United States of America</option>
								<option value="2818389">Afghanistan</option>
										<option value="2818390">Albania</option>
										<option value="2818391">Algeria</option>
										<option value="2818392">American Samoa</option>
										<option value="2818393">Andorra</option>
										<option value="2818394">Angola</option>
										<option value="2818395">Anguilla</option>
										<option value="2818396">Antigua & Barbuda</option>
										<option value="2818397">Argentina</option>
										<option value="2818398">Armenia</option>
										<option value="2818399">Aruba</option>
										<option value="2818400">Australia</option>
										<option value="2818401">Austria</option>
										<option value="2818402">Azerbaijan</option>
										<option value="2818403">Azores</option>
										<option value="2818404">Bahamas</option>
										<option value="2818405">Bahrain</option>
										<option value="2818406">Bangladesh</option>
										<option value="2818407">Barbados</option>
										<option value="2818408">Belarus</option>
										<option value="2818409">Belgium</option>
										<option value="2818410">Belize</option>
										<option value="2818411">Benin</option>
										<option value="2818412">Bermuda</option>
										<option value="2818413">Bhutan</option>
										<option value="2818414">Bolivia</option>
										<option value="2818415">Bonaire</option>
										<option value="2818416">Bosnia & Herzegovina</option>
										<option value="2818417">Botswana</option>
										<option value="2818418">Brazil</option>
										<option value="2818419">British Indian Ocean Ter</option>
										<option value="2818420">Brunei</option>
										<option value="2818421">Bulgaria</option>
										<option value="2818422">Burkina Faso</option>
										<option value="2818423">Burundi</option>
										<option value="2818424">Cambodia</option>
										<option value="2818425">Cameroon</option>
										<option value="2818426">Canada</option>
										<option value="2818427">Canary Islands</option>
										<option value="2818428">Cape Verde</option>
										<option value="2818429">Cayman Islands</option>
										<option value="2818430">Central African Republic</option>
										<option value="2818431">Chad</option>
										<option value="2818432">Channel Islands</option>
										<option value="2818433">Chile</option>
										<option value="2818434">China</option>
										<option value="2818435">Christmas Island</option>
										<option value="2818436">Cocos Island</option>
										<option value="2818437">Colombia</option>
										<option value="2818438">Comoros</option>
										<option value="2818439">Congo</option>
										<option value="2818440">Congo Democratic Rep</option>
										<option value="2818441">Cook Islands</option>
										<option value="2818442">Costa Rica</option>
										<option value="2818443">Cote D'Ivoire</option>
										<option value="2818444">Croatia</option>
										<option value="2818445">Cuba</option>
										<option value="2818446">Curacao</option>
										<option value="2818447">Cyprus</option>
										<option value="2818448">Czech Republic</option>
										<option value="2818449">Denmark</option>
										<option value="2818450">Djibouti</option>
										<option value="2818451">Dominica</option>
										<option value="2818452">Dominican Republic</option>
										<option value="2818453">East Timor</option>
										<option value="2818454">Ecuador</option>
										<option value="2818455">Egypt</option>
										<option value="2818456">El Salvador</option>
										<option value="2818457">Equatorial Guinea</option>
										<option value="2818458">Eritrea</option>
										<option value="2818459">Estonia</option>
										<option value="2818460">Ethiopia</option>
										<option value="2818461">Falkland Islands</option>
										<option value="2818462">Faroe Islands</option>
										<option value="2818463">Fiji</option>
										<option value="2818464">Finland</option>
										<option value="2818465">France</option>
										<option value="2818466">French Guiana</option>
										<option value="2818467">French Polynesia</option>
										<option value="2818468">French Southern Ter</option>
										<option value="2818469">Gabon</option>
										<option value="2818470">Gambia</option>
										<option value="2818471">Georgia</option>
										<option value="2818472">Germany</option>
										<option value="2818473">Ghana</option>
										<option value="2818474">Gibraltar</option>
										<option value="2818475">Great Britain</option>
										<option value="2818476">Greece</option>
										<option value="2818477">Greenland</option>
										<option value="2818478">Grenada</option>
										<option value="2818479">Guadeloupe</option>
										<option value="2818480">Guam</option>
										<option value="2818481">Guatemala</option>
										<option value="2818482">Guernsey</option>
										<option value="2818483">Guinea</option>
										<option value="2818484">Guinea-Bissau</option>
										<option value="2818485">Guyana</option>
										<option value="2818486">Haiti</option>
										<option value="2818487">Honduras</option>
										<option value="2818488">Hong Kong</option>
										<option value="2818489">Hungary</option>
										<option value="2818490">Iceland</option>
										<option value="2818491">India</option>
										<option value="2818492">Indonesia</option>
										<option value="2818493">Iran</option>
										<option value="2818494">Iraq</option>
										<option value="2818495">Ireland</option>
										<option value="2818496">Isle of Man</option>
										<option value="2818497">Israel</option>
										<option value="2818498">Italy</option>
										<option value="2818499">Jamaica</option>
										<option value="2818500">Japan</option>
										<option value="2818501">Jersey</option>
										<option value="2818502">Jordan</option>
										<option value="2818503">Kazakhstan</option>
										<option value="2818504">Kenya</option>
										<option value="2818505">Kiribati</option>
										<option value="2818506">Korea North</option>
										<option value="2818507">Korea South</option>
										<option value="2818508">Kuwait</option>
										<option value="2818509">Kyrgyzstan</option>
										<option value="2818510">Laos</option>
										<option value="2818511">Latvia</option>
										<option value="2818512">Lebanon</option>
										<option value="2818513">Lesotho</option>
										<option value="2818514">Liberia</option>
										<option value="2818515">Libya</option>
										<option value="2818516">Liechtenstein</option>
										<option value="2818517">Lithuania</option>
										<option value="2818518">Luxembourg</option>
										<option value="2818519">Macau</option>
										<option value="2818520">Macedonia</option>
										<option value="2818521">Madagascar</option>
										<option value="2818522">Malawi</option>
										<option value="2818523">Malaysia</option>
										<option value="2818524">Maldives</option>
										<option value="2818525">Mali</option>
										<option value="2818526">Malta</option>
										<option value="2818527">Marshall Islands</option>
										<option value="2818528">Martinique</option>
										<option value="2818529">Mauritania</option>
										<option value="2818530">Mauritius</option>
										<option value="2818531">Mayotte</option>
										<option value="2818532">Mexico</option>
										<option value="2818533">Midway Islands</option>
										<option value="2818534">Moldova</option>
										<option value="2818535">Monaco</option>
										<option value="2818536">Mongolia</option>
										<option value="2818537">Montenegro</option>
										<option value="2818538">Montserrat</option>
										<option value="2818539">Morocco</option>
										<option value="2818540">Mozambique</option>
										<option value="2818541">Myanmar</option>
										<option value="2818542">Namibia</option>
										<option value="2818543">Nauru</option>
										<option value="2818544">Nepal</option>
										<option value="2818545">Netherland Antilles</option>
										<option value="2818546">Netherlands</option>
										<option value="2818547">Nevis</option>
										<option value="2818548">New Caledonia</option>
										<option value="2818549">New Zealand</option>
										<option value="2818550">Nicaragua</option>
										<option value="2818551">Niger</option>
										<option value="2818552">Nigeria</option>
										<option value="2818553">Niue</option>
										<option value="2818554">Norfolk Island</option>
										<option value="2818555">Norway</option>
										<option value="2818556">Oman</option>
										<option value="2818557">Pakistan</option>
										<option value="2818558">Palau Island</option>
										<option value="2818559">Palestine</option>
										<option value="2818560">Panama</option>
										<option value="2818561">Papua New Guinea</option>
										<option value="2818562">Paraguay</option>
										<option value="2818563">Peru</option>
										<option value="2818564">Philippines</option>
										<option value="2818565">Pitcairn Island</option>
										<option value="2818566">Poland</option>
										<option value="2818567">Portugal</option>
										<option value="2818568">Puerto Rico</option>
										<option value="2818569">Qatar</option>
										<option value="2818570">Reunion</option>
										<option value="2818571">Romania</option>
										<option value="2818572">Russia</option>
										<option value="2818573">Rwanda</option>
										<option value="2818574">Saipan</option>
										<option value="2818575">Samoa</option>
										<option value="2818576">Samoa American</option>
										<option value="2818577">San Marino</option>
										<option value="2818578">Sao Tome & Principe</option>
										<option value="2818579">Saudi Arabia</option>
										<option value="2818580">Senegal</option>
										<option value="2818581">Serbia</option>
										<option value="2818582">Serbia & Montenegro</option>
										<option value="2818583">Seychelles</option>
										<option value="2818584">Sierra Leone</option>
										<option value="2818585">Singapore</option>
										<option value="2818586">Slovakia</option>
										<option value="2818587">Slovenia</option>
										<option value="2818588">Solomon Islands</option>
										<option value="2818589">Somalia</option>
										<option value="2818590">South Africa</option>
										<option value="2818591">South Sudan</option>
										<option value="2818592">Spain</option>
										<option value="2818593">Sri Lanka</option>
										<option value="2818594">St Barthelemy</option>
										<option value="2818595">St Eustatius</option>
										<option value="2818596">St Helena</option>
										<option value="2818597">St Kitts-Nevis</option>
										<option value="2818598">St Lucia</option>
										<option value="2818599">St Maarten</option>
										<option value="2818600">St Pierre & Miquelon</option>
										<option value="2818601">St Vincent & Grenadines</option>
										<option value="2818602">Sudan</option>
										<option value="2818603">Suriname</option>
										<option value="2818604">Swaziland</option>
										<option value="2818605">Sweden</option>
										<option value="2818606">Switzerland</option>
										<option value="2818607">Syria</option>
										<option value="2818608">Tahiti</option>
										<option value="2818609">Taiwan</option>
										<option value="2818610">Tajikistan</option>
										<option value="2818611">Tanzania</option>
										<option value="2818612">Thailand</option>
										<option value="2818613">Togo</option>
										<option value="2818614">Tokelau</option>
										<option value="2818615">Tonga</option>
										<option value="2818616">Trinidad & Tobago</option>
										<option value="2818617">Tunisia</option>
										<option value="2818618">Turkey</option>
										<option value="2818619">Turkmenistan</option>
										<option value="2818620">Turks & Caicos Is</option>
										<option value="2818621">Tuvalu</option>
										<option value="2818622">Uganda</option>
										<option value="2818623">Ukraine</option>
										<option value="2818624">United Arab Emirates</option>
										<option value="2818625">United Kingdom</option>
										<option value="2818626">United States of America</option>
										<option value="2818627">Uruguay</option>
										<option value="2818628">Uzbekistan</option>
										<option value="2818629">Vanuatu</option>
										<option value="2818630">Vatican City State</option>
										<option value="2818631">Venezuela</option>
										<option value="2818632">Vietnam</option>
										<option value="2818633">Virgin Islands (Brit)</option>
										<option value="2818634">Virgin Islands (USA)</option>
										<option value="2818635">Wake Island</option>
										<option value="2818636">Wallis & Futana Is</option>
										<option value="2818637">Yemen</option>
										<option value="2818638">Zambia</option>
										<option value="2818639">Zimbabwe</option>
									</select></span>
							</div>
							<div>
								<span class="label"><label for="State">State:</label></span>
								<span><select name="cm-fo-ajyjyk">
										<option value="2818640">Alabama</option>
										<option value="2818641">Alaska</option>
										<option value="2818642">Arizona</option>
										<option value="2818643">Arkansas</option>
										<option value="2818644">California</option>
										<option value="2818645">Colorado</option>
										<option value="2818646">Connecticut</option>
										<option value="2818647">Delaware</option>
										<option value="2818648">District of Columbia</option>
										<option value="2818649">Florida</option>
										<option value="2818650">Georgia</option>
										<option value="2818651">Hawaii</option>
										<option value="2818652">Idaho</option>
										<option value="2818653">Illinois</option>
										<option value="2818654">Indiana</option>
										<option value="2818655">Iowa</option>
										<option value="2818656">Kansas</option>
										<option value="2818657">Kentucky</option>
										<option value="2818658">Louisiana</option>
										<option value="2818659">Maine</option>
										<option value="2818660">Maryland</option>
										<option value="2818661">Massachusetts</option>
										<option value="2818662">Michigan</option>
										<option value="2818663">Minnesota</option>
										<option value="2818664">Mississippi</option>
										<option value="2818665">Missouri</option>
										<option value="2818666">Montana</option>
										<option value="2818667">Nebraska</option>
										<option value="2818668">Nevada</option>
										<option value="2818669">New Hampshire</option>
										<option value="2818670">New Jersey</option>
										<option value="2818671">New Mexico</option>
										<option value="2818672">New York</option>
										<option value="2818673">North Carolina</option>
										<option value="2818674">North Dakota</option>
										<option value="2818675">Ohio</option>
										<option value="2818676">Oklahoma</option>
										<option value="2818677">Oregon</option>
										<option value="2818678">Pennsylvania</option>
										<option value="2818679">Rhode Island</option>
										<option value="2818680">South Carolina</option>
										<option value="2818681">South Dakota</option>
										<option value="2818682">Tennessee</option>
										<option value="2818683">Texas</option>
										<option value="2818684">Utah</option>
										<option value="2818685">Vermont</option>
										<option value="2818686">Virginia</option>
										<option value="2818687">Washington</option>
										<option value="2818688">West Virginia</option>
										<option value="2818689">Wisconsin</option>
										<option value="2818690">Wyoming</option>
									</select></span>
							</div>
						</div>
						<div>
							<span class="button"><input type="submit" value="Subscribe" /></span>
						</div>
					</form>
					<?php edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>	
					<div class="social">
						<?php
						if (function_exists(do_sociable())) {
							do_sociable();
						}
						?>
					</div>
				</div><!--end entry-->
			</div><!--end post-->
	<?php endwhile; ?>
	</div>
<div id="sidebar-about" class="sidebar section">
	<?php
	wp_nav_menu(array(
		'theme_location' => 'about_nav',
		'container' => 'div',
		'container_id' => 'about-nav',
	));
	?>
</div>
	<?php
endif;
require_once('footer.php');