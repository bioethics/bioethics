<?php

require_once('includes/admin.php');
require_once('includes/post-types.php');	
require_once( 'includes/enqueue_scripts.php' );
require_once( 'includes/functions.php' );

// register menus
if (function_exists('register_nav_menus')) {
	register_nav_menus(
			array(
				'primary_nav' => 'Primary Navigation',
				'secondary_nav' => 'Secondary Navigation',
				'footer_nav' => 'Footer Navigation',
				'about_nav' => 'About Pages Navigation'
			)
	);
}

add_theme_support( 'post-thumbnails' );

if (function_exists('add_image_size')) {
	add_image_size('journal_medium', 9999, 190, false); //300 pixels wide (and unlimited height)
	add_image_size('journal_small', 9999, 76, false); //76 pixels wide (and unlimited height)
  add_image_size('home_blog', 300, 300, false); 
}

//register sidebars
function my_register_sidebars() {

	register_sidebar(array(
			'name' => 'Blog Sidebar',
			'id' => 'blog-sidebar',
			'description' => 'Widgets in this area will be shown on the right-hand side of the blog.',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>'
	));
	register_sidebar(array(
			'name' => 'Tag Sidebar',
			'id' => 'tag-sidebar',
			'description' => 'Widgets in this area will be shown on the tags page.',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>'
	));
}

add_action('widgets_init', 'my_register_sidebars');

// Rename Admin Links
function change_post_menu_label() {
// global $menu;
// global $submenu;  
// $menu[28][0] = 'AJOB';
// $submenu['edit.php'][5][0] = 'All Posts';
// echo '';
}

add_action('admin_menu', 'change_post_menu_label');

// remove unused menu items
function remove_menus() {
	remove_menu_page('link-manager.php');
}

add_action('admin_menu', 'remove_menus', 102);

// AJOB admin columns
function issues_edit_columns($columns) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'cover_image' => '',
		'title' => 'Title',
        'publish_date' => 'Publish Date',
//		'volume' => 'Volume',
//		'number' => 'Number',
		'journal' => 'Edition',
		'date' => 'Date'
	);
	return $columns;
}

add_filter("manage_edit-issues_columns", "issues_edit_columns");

// AJOB sortable columns
function my_column_register_sortable($columns) {
//	$columns["volume"] = "volume";
//	$columns["number"] = "number";
    $columns['publish_date'] = "publish_date";
	return $columns;
}

add_filter("manage_edit-issues_sortable_columns", "my_column_register_sortable");

// Article admin columns
function articles_edit_columns($columns) {
// $columns["edition"] = "Edition";
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => 'Title',
		'edition' => 'Edition',
		'article_type' => 'Article Type',
		'categories' => 'Categories',
		'tags' => 'Tags',
		'date' => 'Date'
	);
	return $columns;
}

add_filter("manage_edit-articles_columns", "articles_edit_columns");

// Author admin columns
function author_edit_columns($columns) {
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => 'Title',
		'degree' => 'Degree',
		'institution' => 'Institution',
		'date' => 'Date'
	);
	return $columns;
}

add_filter("manage_edit-authors_columns", "author_edit_columns");

// Logic for all custom admin columns (across all post-types)
function my_custom_columns($column) {
	global $post;
	switch ($column) {
		case "cover_image":
			$img = get_field('cover_image');
			echo edit_post_link('<img src="'.$img['sizes']['thumbnail'].'">');
			break;
        case "publish_date":
            echo '
            <div class="row-title"><strong>'.
                date('F Y', strtotime(get_field('publish_date'))).
            '</strong></div>'.
            '<div class="row-actions" style="visibility: visible;">Volume '.get_field('volume') . ", Number ".get_field('number').'</div>';
            break;
//		case "volume":
//			echo get_field('volume');
//			break;
//		case "number":
//			echo get_field('number');
//			break;
		case "journal":
			$terms = get_the_terms($post->ID, 'editions');
			$links = array();
			foreach ($terms as $term) {
				$links[] = $term->name;
			}
			echo join(", ", $links);
			break;
		case "edition":
			$data = get_field('ajob_issue');
			echo $data->post_title;
			break;
		case 'degree':
			echo get_field('degree');
			break;
		case 'institution':
			echo get_field('institution');
			break;
		case 'article_type':
			$terms = get_the_terms($post->ID, 'article_types');
			$links = array();
			if (count($terms) > 0):
				foreach ($terms as $term) {
					$links[] = $term->name;
				}
				echo join(", ", $links);
			endif;
			break;
	}
}

add_action("manage_posts_custom_column", "my_custom_columns");

// admin filters for all custom taxonomies
function my_restrict_manage_posts() {
	global $typenow;

	if ($typenow != "page" && $typenow != "post") {
		$filters = get_taxonomies(array('object_type' => array($typenow)));

		foreach ($filters as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
			echo "<option value=''>Show All $tax_name</option>";
			foreach ($terms as $term) {
				echo '<option value=' . $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '', '>' . $term->name . ' (' . $term->count . ')</option>';
			}
			echo "</select>";
		}
	}
}

add_action('restrict_manage_posts', 'my_restrict_manage_posts');

// custom admin css
function customAdmin() {
	echo '<!-- custom admin css -->
<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/admin.css" />
<!-- /end custom adming css -->';
}

add_action('admin_head', 'customAdmin');


//custom journal permalink
//add_filter('post_link', 'journal_permalink', 10, 3);
//add_filter('post_type_link', 'journal_permalink', 10, 3);
//function journal_permalink($permalink, $post_id, $leavename) {
//	if (strpos($permalink, '%editions%') === FALSE) return $permalink;
// 
//        // Get post
//        $post = get_post($post_id);
//        if (!$post) return $permalink;
//
//        $volume = get_field('volume', $post->ID);
//        $number = get_field('number', $post->ID);
//
//        // Get taxonomy terms
//        $terms = wp_get_object_terms($post->ID, 'editions');	
//        if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])): 
//          $edition_slug = $terms[0]->slug;
//        else:
//          $edition_slug = 'ajob';
//        endif;
//        $find = array(
//            '%edition%',
//            '%volume%',
//            '%issue%',
//            $post->post_name.'/'
//        );
//        $replace = array(
//            $edition_slug,
//            $volume,
//            $number,
//            ''
//        );
//        $permalink = str_replace($find, $replace, $permalink); 
//	return $permalink;
//}


// function performance($text) {
// 	$stat = sprintf('%d queries in %.3f seconds, using %.2fMB memory', get_num_queries(), timer_stop(0, 3), memory_get_peak_usage() / 1024 / 1024
// 	);
// 	echo $text . ' ' . $stat;
// }

// add_action('admin_footer_text', 'performance', 20);


add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
  if($query->is_category() || $query->is_tag()) {
    $post_type = get_query_var('post_type');
	if($post_type && is_string($post_type)):
	    $post_type = array($post_type,'nav_menu_item','jobs');
	else:
	    $post_type = array('post','articles','events','news', 'links','books','nav_menu_item','jobs');
	endif;
    $query->set('post_type',$post_type);
	return $query;
    }
}
function change_search_url_rewrite() {
	if ( is_search() && ! empty( $_GET['s'] ) ) {
		wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
		exit();
	}
}
add_action( 'template_redirect', 'change_search_url_rewrite' );

// journal/articles relationship cleanup to only show articles associated with specific journal
function journal_issue_query( $args, $field, $post )
{
 		$args['meta_query'] = array(
			array(
				'key' => 'ajob_issue',
				'value' => $post->ID
			)
   	);
    return $args;
}
 

// article
add_filter('acf/fields/relationship/query/key=field_4f19650c6f1fb', 'journal_issue_query', 10, 3);

// correspondence
add_filter('acf/fields/relationship/query/key=field_4f22200a16206', 'journal_issue_query', 10, 3);

// book review
add_filter('acf/fields/relationship/query/key=field_4f221f35183ea', 'journal_issue_query', 10, 3);

// editorial
add_filter('acf/fields/relationship/query/key=field_4f22200a16705', 'journal_issue_query', 10, 3);

// in focus
add_filter('acf/fields/relationship/query/key=field_4f3db00aaa530', 'journal_issue_query', 10, 3);

// column
add_filter('acf/fields/relationship/query/key=field_4f22200a15c4a', 'journal_issue_query', 10, 3);

// reflection
add_filter('acf/fields/relationship/query/key=field_1', 'journal_issue_query', 10, 3);

// articles
add_filter('acf/fields/relationship/query/key=field_4', 'journal_issue_query', 10, 3);


// event archive date 
 add_filter( 'pre_get_posts', 'filter_upcomingevents' );
 function filter_upcomingevents( $query ) {
  if ( ! is_admin()  && isset($query->query['post_type']) && $query->query['post_type'] == 'events'  && $query->is_main_query()) {
	 	
      $today = date('Ymd', strtotime('-1 day'));
      $meta_query = array(
         array(
            'key'  => 'end_date' ,
            'value'  => $today,                              
            'compare'  => '>',
            'type' => 'DATE'
         )
      );
      $query->set( 'meta_type', 'DATE' );
      $query->set( 'posts_per_page', '30');
      $query->set( 'meta_key', 'start_date' );
      $query->set( 'orderby', 'meta_value_num' );
      $query->set( 'order', 'ASC' );
      $query->set( 'meta_query', $meta_query );
  }
  return $query;
 }

// update gravity form date fields to always return yyyymmdd format
add_filter("gform_save_field_value", "save_field_value", 10, 4);
function save_field_value($value, $lead, $field, $form){
	
	if($field["type"]=='date'):
  	$value = date('Ymd', strtotime($value));
  endif;
  return $value;
}


// update gravity form data to set end date same as start_date if end_date is empty
add_filter("gform_pre_submission_filter", "update_end_date");
function update_end_date($form){

  if($form['id']==2 && $_POST['input_16']==""):
    $_POST['input_16'] = $_POST['input_15'];
  endif;
    
    return $form;
}

// update acf form data to set end date same as start_date if end_date is empty
add_filter('acf/update_value/key=field_4f205d702e926', 'update_acf_end_date', 10, 3);

function update_acf_end_date($value, $post_id, $field ){
  if($value == ''):
    $value = $_POST['fields']['field_4f205d702e43a'];
  endif;
  return $value;
}
/**
 * Clear Transients when post is saved
 *
 * @param int $post_id The ID of the post.
 */
function clear_transients( $post_id ) {
  delete_transient( 'latest_post' );

  $post_type = $_POST['post_type'];
  // Actions if updating journals
  if($post_type=='issues'):
    delete_transient( 'nav_journal_data' );
  // Actions if updating events
  elseif($post_type == 'events'):
    delete_transient( 'footer_events' );
  // Actions if updating jobs
  elseif($post_type == 'jobs'):
    delete_transient( 'current_jobs' );
    delete_transient( 'current_jobs_count' );
  endif;
}
add_action( 'save_post', 'clear_transients' );
