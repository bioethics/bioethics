<?php require_once('header.php'); ?>
<div id="single-content" class="section">
	
<?php if (have_posts()) : ?>
	<?php
	while (have_posts()) : the_post(); 
		$article_fields = get_fields(); 
		$journal_id = $article_fields['ajob_issue']->ID;
		$journal_fields = get_fields($journal_id);
		?>
	<h2 class="title"><a href="<?= get_permalink($journal_id); ?>"><?php 
		
		$tax = wp_get_post_terms($journal_id,'editions','name');
		echo $tax[0]->name;
	?>.</a></h2>
	<?php
		
	?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
			<div class="post-header">
				<h3 class="post-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php the_title(); ?>
					</a>
				</h3>
			</div><!--end post-header-->
			<div class="post_body entry">
				<div class="info">
					<p class="article_author"><span class="author">By <?php 
				
						$pg = explode("-", $article_fields['page_number'], 2);
						$firstpage = $pg[0];
				
						echo $article_fields['primary_author'];
						if(isset($article_field['additional_authors']))
							echo ', ',$article_field['additional_authors'];
					?></span>
						<span class="pages">
							Pages: <?=$article_fields['page_number'];?>
						</span>
					</p>
				</div>
				<?php 
				if($article_fields['full_text'] !=0 )
					echo $article_fields['full_text'];
				else
					echo $article_fields['abstract'];
				?>
				<p class="more"><a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$journal_fields['isbn']?>&volume=<?=$journal_fields['volume']?>&issue=<?=$journal_fields['number'];?>&spage=<?=$firstpage?>" class="button">View Full Text</a></p>
				<?php 
				// Open Peer Commentaries
				if(is_array($article_fields['open_peer_commentary']) && count($article_fields['open_peer_commentary'])>0): ?>
				<div id="opt_detail">
				<h3>Open Peer Commentaries.</h3>
				<ul>
				<?php foreach($article_fields['open_peer_commentary'] as $opt): 
					$pg = explode("-", $opt['page_number'], 2);
					$firstpage = $pg[0];
					?>
					<li class="title">
						<a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$journal_fields['isbn']?>&volume=<?=$journal_fields['volume']?>&issue=<?=$journal_fields['number'];?>&spage=<?=$firstpage?>" target="_blank"><?= $opt['title']?></a><span class="author"> <?= $opt['authors']?></span>
					</li>
				<?php endforeach; ?>
				</ul>
				</div>
				<?php endif; ?>

				<div class="meta">
					<p><?php
					$utility_text = 'Bookmark the <a href="%1$s" title="Permalink to %2$s" rel="bookmark">permalink</a>.';
					printf(
						$utility_text,
						esc_url( get_permalink() ),
						the_title_attribute( 'echo=0' )
					);
					edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); 
					?></p>
				</div>
				<div class="social">
					<?php 
					if( function_exists( do_sociable() ) ){ do_sociable(); } 
					?>
				</div>
				<div id="comments">
					<?php comments_template( '', true ); ?>
				</div>
			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; ?>

<?php else : ?>
	<h1>No Posts!</h1>	
<?php endif; ?>

</div>
	<div id="sidebar-article" class="sidebar section">
		<?php 
		$permalink = get_permalink($journal_id);
		?>
		
		<div id="article_meta">
			<a href="<?=$permalink;?>"><img src="<?=$journal_fields['cover_image']['sizes']['journal_medium'];?>"></a>
			<h2 class="title"><a href="<?= $permalink; ?>">Volume <?=$journal_fields['volume']; ?>, Issue <?= $journal_fields['number']?> <br/> <?php
			echo date('F Y',strtotime($journal_fields['publish_date']));
			?></h2>
			<ul>
				<li><a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$journal_fields['isbn']?>&volume=<?=$journal_fields['volume']?>&issue=<?=$journal_fields['number'];?>" target="_blank">Get This Issue</a></li>
				<li><a href="/editions/<?= $tax[0]->slug ?>">Past Issues</a></li>
				<!-- <li><a href="/about/editors/">The Editors</a></li>
				<li><a href="/about/editorial-board/">Editorial Board</a></li> -->
			</ul>
		</div>
				
<?php
		$i = 0;
		if(isset($journal_fields['target_articles']) && $journal_fields['target_articles'] !=0): 
			$i++; 
		?>
					
		<!-- Articles Section -->
		<div id="editorial" class="onecol <?php if($i==1 || $i%4==0) echo " first";?> list">
			<h2 class="title">Target Articles.</h2>
			<?php 
			foreach($journal_fields['target_articles'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>
		
		<?php endif; 
		
		if(isset($journal_fields['column']) && $journal_fields['column'] !=0): $i++;?>
			<!-- correspondence -->
			<div id="essayarticles">
				<h2 class="title">Essays &amp; Articles.</h2>
				<?php foreach($journal_fields['column'] as $data): 
					format_articles($data);
				endforeach; ?>
			</div>
		<?php endif; 
		
		if(isset($journal_fields['editorial']) && $journal_fields['editorial'] !=0): $i++;?>
					
		<!-- Editorial Section -->
		<div id="editorial" class="onecol <?php if($i==1 || $i%4==0) echo " first";?> list">
			<h2 class="title">Editorial.</h2>
			<?php foreach($journal_fields['editorial'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>
		
		<?php endif; 
		
	if(isset($journal_fields['in_focus']) && $journal_fields['in_focus'] !=0):$i++;?>
		
		<!-- In Focus -->
		<div id="in_focus">
			<h2 class="title">In Focus.</h2>
		<?php foreach($journal_fields['in_focus'] as $data): 
			format_articles($data);
		endforeach; ?>
		</div>
		
	<?php endif; 
	
	if(isset($journal_fields['correspondence']) && $journal_fields['correspondence'] !=0): $i++;?>
		
		<!-- correspondence -->
		<div id="correspondence">
			<h2 class="title">Correspondence.</h2>
			<?php foreach($journal_fields['correspondence'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>

	<?php endif; 
	
	if(isset($journal_fields['book_review']) && $journal_fields['book_review'] !=0): $i++;?>
		<!-- book review -->
		<div id="book_reviews">
			<h2 class="title">Book Reviews.</h2>
			<?php foreach($journal_fields['book_review'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>		
	<?php endif; 
	
	if(isset($journal_fields['reflection']) && $journal_fields['reflection'] !=0): $i++;?>
		<!-- book review -->
		<div id="reflection">
			<h2 class="title">Reflection.</h2>
			<?php foreach($journal_fields['reflection'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>		
	<?php endif; ?>
	</div>
<?php
require_once('footer.php');
