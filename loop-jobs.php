<?php
global $jobs;

if ( $jobs) : 
	$lastmonth = null;
	$i = 1;
	$cols = 3;
	while ( $jobs->have_posts() ) : $jobs->the_post(); 
		$deadline = get_field('application_deadline');
		$deadline_text = get_field('application_deadline_text');
	
		//JOBS	

		if($i==1) echo '<div class="row">';
		?>
<div id="job-<?php the_ID(); ?>" <?php post_class('list-post col-'.$i); ?>>

		<?php if($deadline): ?>
			<h3>Deadline: 
				<?= ($deadline_text!='') ? $deadline_text : date('F jS, Y',  strtotime($deadline))?>
			</h3>
		<?php endif; ?>
		<!-- <h3 class="date">Posted:  -->
		<!-- 	<?php echo get_the_date('F d, Y');?> -->
		<!-- </h3> -->
		<h4 class="title"><a href="<?php the_permalink() ?>" title="Bioethics Job: <?php the_field('institution_name') ?> : <?php the_field('title') ?>"><?php the_title() ?></a></h4>
		<h5><?php the_field('institution_name') ?></h5>
		<h5 class="location">
		<?php
			$location = array();
			$city =	get_field('city');
			if($city && $city !='') $location[] = $city;
			$state = get_field('state');
			if($state && $state !='') $location[] = $state;
			$province = get_field('province');
			if($province && $province!='') $location[] = $province;
			$country = get_field('country');
			if($country && $country !='') $location[] = $country;

			echo implode(', ',$location);
		?>
		</h5>
		<div class="description">
			<p><?php the_field('short_description') ?></p>
		</div>
		<p class="more"><a href="<?php the_permalink() ?>" title="View more job info for <?php the_title() ?>">Details</a></p>
	<?php

	
	
		edit_post_link('Edit this', ' <p class="edit-link">', '</p>');
		if($i==$cols):
			$i = 1;
			echo '</div> <!-- /.row -->';
		else:
			$i++;
		endif;
		// $lastmonth = $thismonth;
		?>	
	</div><!--end list-post-->
	<?php 
	endwhile; /* rewind or continue if all posts have been fetched */ ?>
<?php else : ?>
<h1>No Posts</h1>	
<?php endif; ?>
