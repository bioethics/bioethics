<?php 
require_once('header.php'); 
$jobs = get_current_jobs();

$previous = get_adjacent_post( false, '', true,'' );
$pages_total = intval($jobs->max_num_pages);
$paged_var = intval(get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1);

?>
<div id="list-content">
	<?php 
	if ( $jobs) : ?>
		<h2 class="title"> Bioethics Jobs. <span><a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs" id="subnav_current">Current Bioethics Jobs</a> | <a href="<?php bloginfo('wpurl')?>/submit_job" id="subnav-submit">Submit a Job</a></span></h2>
		<?php require_once('loop-jobs.php') ?>


		<div id="pagination">
			<span class="nav-old">
			<?php 
				if($paged_var !== $pages_total){
					next_posts_link( '<span class="meta-nav">&larr;</span> Next' );
				}
			?>
			</span>
			<span class="nav-new"><?php previous_posts_link( 'Previous <span class="meta-nav">&rarr;</span>' ); ?></span>
		</div>
	<?php else : ?>
		<p>No posts found.</p>
	<?php endif; 
?>
</div><!--end content-->
<?php require_once('footer.php');