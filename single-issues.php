<?php
require_once('header.php'); 
if (have_posts()) : 
	while (have_posts()) : the_post();

$info = get_fields();
$terms = wp_get_post_terms(get_the_ID(), 'editions');
$terms = $terms[0];
$terms = single_issue($terms,$info);

?>

<div id="journal">
	<div id="current-issue" class="section <?=$terms->class?>">
		<?php $image = $info['cover_image']; ?>
		<div class="left" id="current-issue-cover">
			<img src="<?php echo $image['sizes']['journal_medium']; ?>" alt="Vol. <?= $info['volume']; ?> No. <?= $info['number']; ?> | <?= date('F Y',strtotime($info['publish_date'])) ?>" />
		</div>
		<div id="current-issue-details" class="left">
			<h3>Vol. <?= $info['volume']; ?> No. <?= $info['number']; ?> | <?= date('F Y',strtotime($info['publish_date'])) ?></h3>
			<h4>ISBN: <?= $info['isbn']; ?></h4>
		</div>
		<div class="last">
			<ul class="column01 left">
				<?php if(isset($info['editorial']) && $info['editorial'] !=0):?>
					<li><a href="#editorial">Editorial</a></li>
				<?php endif; ?>
				<?php if(isset($info['target_articles']) && $info['target_articles'] !=0):?>
					<li><a href="#target_articles">Target Articles</a></li>
				<?php endif; ?>
				<?php if(isset($info['book_review']) && $info['book_review'] !=0):?>
					<li><a href="#book_reviews">Book Reviews</a></li>
				<?php endif; ?>
				<?php if(isset($info['column']) && $info['column'] !=0):?>
					<li><a href="#essayarticles">Essay/Articles</a></li>
				<?php endif; ?>
				<?php if(isset($info['in_focus']) && $info['in_focus'] !=0):?>
					<li><a href="#in_focus">In Focus</a></li>
				<?php endif; ?>
				<?php if(isset($info['correspondence']) && $info['correspondence'] !=0):?>
					<li><a href="#correspondence">Correspondence</a></li>
				<?php endif; ?>
				<?php if(isset($info['reflection']) && $info['reflection'] !=0):?>
					<li><a href="#reflection">Reflection</a></li>
				<?php endif; ?>
			</ul>
			<ul class="column02 left">
				<li><a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$info['isbn']?>&volume=<?=$info['volume']?>&issue=<?=$info['number'];?>" target="_blank">Get This Issue</a></li>
				<li><a href="/editions/<?=$terms->slug ?>">Past Issues</a></li>
				<!-- <li><a href="/about/editors/">The Editors</a></li>
				<li><a href="/about/editorial-board/">Editorial Board</a></li> -->
			</ul>
		</div>
	</div>


<?php 
	if($info['editorial']!=0): 
?>
	<h2 class="title" id="editorial">editorial.</h2>
<?php
	$i = 0;
	foreach($info['editorial'] as $edi):
		$edi_info = get_fields($edi->ID);
		$permalink = get_permalink($edi->ID);
?>
	<div class="editorial-article editorial-articles-0<?php echo ($i==0)? '1' : '2' ?> section">
		<div class="onecol first">
			<h3 id="featured-article-title"><a href="<?= $permalink; ?>" title="<?=$edi->post_title;?>"><?=$edi->post_title;?></a></h3>
			<h6 class="author"><?php 
				echo $edi_info['primary_author']; 
				if($edi_info['additional_authors']) echo ", ".$edi_info['additional_authors'];
			?></h6>
		</div>
		<div class="article-details onecol">
			<p><?php echo substr(strip_tags($edi_info['abstract']),0,400).'...';?></p>
			<?php
				$pg = explode("-", $edi_info['page_number'], 2);
				$firstpage = $pg[0];

			?>
			<a href="<?= $permalink; ?>" class="button" title="<?=$edi->post_title;?>">Click for More</a>
		</div>
	</div>
<?php endforeach; ?>
<?php 
		endif; 
?>

<?php if($info['target_articles']!=0): ?>
	<h2 class="title" id="target_articles">target articles.</h2>
<?php
	$i = 0;
	foreach($info['target_articles'] as $ta):
		$ta_info = get_fields($ta->ID);
		$permalink = get_permalink($ta->ID);
?>
	
	<div class="target-article target-articles-0<?php echo ($i==0)? '1' : '2' ?> section">
		<div class="onecol first">
<!-- <h5 class="date">February 12, 2012 12:00 am</h5> -->
			<h3 id="featured-article-title"><a href="<?= $permalink; ?>" title="<?=$ta->post_title;?>"><?=$ta->post_title;?></a></h3>
			<h6 class="author"><?php 
				echo $ta_info['primary_author']; 
				if($ta_info['additional_authors']) echo ", ".$ta_info['additional_authors'];
			?></h6>
		</div>
		<div class="article-details onecol">
			<p><?php echo substr(strip_tags($ta_info['abstract']),0,400).'...';?></p>
			<?php
				$pg = explode("-", $ta_info['page_number'], 2);
				$firstpage = $pg[0];

			?>
<!--			<a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$info['isbn']?>&volume=<?=$info['volume']?>&issue=<?=$info['number'];?>&spage=<?=$firstpage?>" class="button" target="_blank">Click for More</a>-->
			<a href="<?= $permalink; ?>" class="button" title="<?=$ta->post_title;?>">Click for More</a>
		</div>
		
		
		<?php 
		// if has open peer commentary
		if(is_array($ta_info['open_peer_commentary']) && count($ta_info['open_peer_commentary'])>0): ?>
		<div class="onecol list border">
			<h4>Open Peer Commentary</h4>
			<?php foreach($ta_info['open_peer_commentary'] as $opt): 
				$pg = explode("-", $opt['page_number'], 2);
				$firstpage = $pg[0];
				?>
			<div class="item">
				<h5 class="title"><a href="http://www.tandfonline.com/openurl?genre=article&issn=<?=$info['isbn']?>&volume=<?=$info['volume']?>&issue=<?=$info['number'];?>&spage=<?=$firstpage?>" target="_blank"><?= $opt['title']?></a><span class="author"> <?= $opt['authors']?></span></h5>
			</div>
			<?php endforeach; ?>
		</div>
		<?php endif; //end if open peer commentary ?>
		
		
	</div>
<?php	
	$i++;
	endforeach;
endif; // end target articles ?>
	<div id="bottom-section" class="section">
		
		
		<?php 
		

		
		
	if(isset($info['in_focus']) && $info['in_focus'] !=0):$i++;?>
		
		<!-- In Focus -->
		<div id="in_focus" class="onecol <?php
			if($i==1 || $i%4==0) echo " first";
			if($i%3==0) echo " last";
		?> list">
			<h2 class="title">In Focus.</h2>
		<?php foreach($info['in_focus'] as $data): 
			format_articles($data);
		endforeach; ?>
		</div>
		
	<?php endif; ?>
	<?php if(isset($info['correspondence']) && $info['correspondence'] !=0): $i++;?>

		
		<!-- correspondence -->
		<div id="correspondence" class="onecol <?php
			if($i==1 || $i%4==0) echo " first";
			if($i%3==0)echo " last";
		?> list">
			<h2 class="title">Correspondence.</h2>
			<?php foreach($info['correspondence'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>

		
	<?php endif; ?>
			
	<?php if(isset($info['book_review']) && $info['book_review'] !=0): $i++;?>
		<!-- book review -->
		<div id="book_reviews" class="onecol <?php
			if($i==1 || $i%4==0) echo " first";
			if($i%3==0)echo " last";
		?> list">
			<h2 class="title">Book Reviews.</h2>
			<?php foreach($info['book_review'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>		
	<?php endif; ?>
		
		
	<?php if(isset($info['column']) && $info['column'] !=0): $i++;?>
		<!-- column -->
		<div id="essayarticles" class="onecol <?php
			if($i==1 || $i%4==0) echo " first";
			if($i%3==0)echo " last";
		?> list">
			<h2 class="title">Essays &amp; Articles.</h2>
			<?php foreach($info['column'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>
	<?php endif; ?>

	<?php if(isset($info['reflection']) && $info['reflection'] !=0): $i++;?>
		<!-- reflection -->
		<div id="reflection" class="onecol <?php
			if($i==1 || $i%4==0) echo " first";
			if($i%3==0)echo " last";
		?> list">
			<h2 class="title">Reflection.</h2>
			<?php foreach($info['reflection'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>
	<?php endif; ?>

	<?php if(isset($info['articles']) && $info['articles'] !=0): $i++;?>
		<!-- articles -->
		<div id="articles" class="onecol <?php
			if($i==1 || $i%4==0) echo " first";
			if($i%3==0)echo " last";
		?> list">
			<h2 class="title">Articles.</h2>
			<?php foreach($info['articles'] as $data): 
				format_articles($data);
			endforeach; ?>
		</div>
	<?php endif; ?>

		
		
	</div>
</div>

<?php endwhile;
	
else : ?>
	<h1>No Posts!</h1>	
<?php endif; 
	require_once('footer.php'); ?>
