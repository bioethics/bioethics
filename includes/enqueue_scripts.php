<?php
/**
 * Enqueue Styles and Scripts
 * Prints css and javascript links
 * @uses wp_enqueue_scripts action hook
*/

if ( ! function_exists( 'bioethics_enqueue_scripts' ) ) :

function bioethics_enqueue_scripts() {
	if ( ! is_admin() ) {
	
		// Main Stylesheet
		wp_enqueue_style(
			'bioethics-style',
			get_bloginfo( 'stylesheet_url' )
		);
		
		// Modernizr
		wp_enqueue_script(
			'modernizr',
			get_template_directory_uri() . 
			'/includes/js/modernizr.min.js'
		);
		
		// Deregister local jQuery and replace with latest Google CDN
		wp_deregister_script( 'jquery' );
		wp_register_script(
			'jquery',
		  	'http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js'
		);
		wp_enqueue_script( 'jquery' );


		// Theme Javascript
		wp_enqueue_script(
			'theme-script',
			get_template_directory_uri() . '/javascripts/theme.js',
			'', 
			'', 
			true # in footer
		);
		//add tinymce js to frontend ONLY if page has form.
		// add_action("gform_enqueue_scripts", "enqueue_custom_script", 10, 2);
		// function enqueue_custom_script($form, $is_ajax){			
		// 	wp_enqueue_script( 
		// 		'my_tiny_mce',
		// 		get_bloginfo('url').'/wp-includes/js/tinymce/tiny_mce.js'
		// 	);
		// }
	}

}

function bioethics_admin_enqueue_scripts(){
	wp_enqueue_script(
		'admin-script',
		get_template_directory_uri() . '/javascripts/admin.js',
		array('jquery'), 
		'1', 
		true # in footer
	);
}
endif;
add_action( 'wp_enqueue_scripts', 'bioethics_enqueue_scripts' );
add_action( 'admin_enqueue_scripts', 'bioethics_admin_enqueue_scripts' );