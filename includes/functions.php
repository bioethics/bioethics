<?php
//if (function_exists('register_options_page')) {
// register_options_page('Global');
// register_options_page('Home Options');
//}
function journal_rename($edition){
  //for journal nav renaming 
  if($edition->edition->slug === 'primary_research' && $edition->meta[volume] >= 5){
    $edition->edition->name = 'AJOB Empirical Bioethics';
  }

  return $edition;
}
function single_issue($terms,$info){
  //for single issue
   $terms->class =  $terms->slug;
  if($terms->slug === 'primary_research'){
    if($info[volume] >= 5){
      $terms->name = 'AJOB Empirical Bioethics';
      $terms->class = 'empirical_bioethics';      
    }

  }
  return $terms;
}
function get_new_journals(){
  $result = get_transient( 'nav_journal_data');
  //delete_transient('nav_journal_data');//only use for development
  if ( false === $result ) {
    global $wp_query;
    $backup = $wp_query;
    $wp_query = NULL;
    $editions_by_term = get_terms('editions');
    $result = array();
    foreach($editions_by_term as $ed):
      $args = array(
        'post_type' => 'issues',
        'posts_per_page' => 1,
        'tax_query' => array(
          array(
            'taxonomy' => 'editions',
            'field' => 'id',
            'terms' => $ed->term_id
          )
        )
      );
      $query = new WP_Query($args);
      $post = $query->post;
      $meta['cover_image'] = get_field('cover_image',$post->ID);
      $meta['volume'] = get_field('volume',$post->ID);
      $meta['number'] = get_field('number',$post->ID);
      $meta['publish_date'] = get_field('publish_date',$post->ID);
      $meta['isbn'] = get_field('isbn');
      $result[$ed->slug] = $post;
      $result[$ed->slug]->meta = $meta;
      $result[$ed->slug]->edition = $ed;
    endforeach;
    ksort($result);
    $result[primary_research] = journal_rename($result[primary_research]);
    $wp_query = $backup;
    set_transient('nav_journal_data', $result, YEAR_IN_SECONDS);
  } 
  return $result;
}

function jobs_filter_where( $where = '' ) {
  // posts in the last 60 days
  //$oldest_post_date = date('Y-m-d', strtotime('-60 days'));
  //$where .= " AND post_date > '" . $oldest_post_date . "'";
  //return $where;
}

function get_current_jobs($count_only = false){
  global $paged;
  if($count_only != false):
    $jobs = get_transient('current_jobs_count');
  else:
    //delete_transient('current_jobs_'. $paged);//only use for development
    $jobs = get_transient('current_jobs_'. $paged);
  endif;
  if(false === $jobs):
    global $wp_query;
    $backup = $wp_query;
    $wp_query = NULL;

    //$today = date('Y-m-d');
    $today = date('Ymd');
    $vars = array();
    $vars['meta_key'] = 'application_deadline';
    $vars['meta_compare'] = '>=';
    $vars['meta_value'] = $today;
    $vars['order'] = 'ASC';
    $vars['orderby'] = 'application_deadline';
    $vars['posts_per_page'] = 9;
    $vars['post_type']    = 'jobs';
    $vars['paged'] = $paged;
    
    if($count_only == true)
      $vars['fields'] = 'ids';

    //add_filter( 'posts_where', 'jobs_filter_where' );
  	$jobs = new WP_Query($vars);
  	// if (function_exists('jobs_filter_where')) :
   //  	remove_filter( 'posts_where', 'jobs_filter_where' );
  	// endif;
  	$wp_query = $backup;
    if($count_only != false):
      $jobs = $jobs->found_posts;
      set_transient('current_jobs_count', $jobs, DAY_IN_SECONDS);
    else:
      set_transient('current_jobs_'.$paged, $jobs, DAY_IN_SECONDS);
    endif;
  endif;
	return $jobs;
}

//// select boxes for category selectors
//function add_single_value_meta_boxes() {
//	foreach ( get_taxonomies( array( 'show_ui' => true ), 'object' ) as $taxonomy ) {
//		if ( !isset($taxonomy->single_value) || !$taxonomy->single_value )
//			continue;
//
//		foreach ( $taxonomy->object_type as $object_type ) {
//			remove_meta_box($taxonomy->name.'div', $object_type, 'side');
//			add_meta_box('tagsdiv-' . $taxonomy->name, $taxonomy->labels->name, 'tax_single_value_meta_box', $object_type, 'side', 'default', array( 'taxonomy' => $taxonomy->name ));		
//		}
//	}
//}
//add_action('add_meta_boxes', 'add_single_value_meta_boxes');
//
//function tax_single_value_meta_box($post, $box) {
//	if ( !isset($box['args']) || !is_array($box['args']) )
//		$args = array();
//	else
//		$args = $box['args'];
//	extract( wp_parse_args($args), EXTR_SKIP );
//	$tax_name = esc_attr($taxonomy);
//	$taxonomy = get_taxonomy($taxonomy);
//	$disabled = !current_user_can($taxonomy->cap->assign_terms) ? 'disabled="disabled"' : '';
//	echo "<select name='tax_input[{$tax_name}]' $disabled>";
//	if ( !isset($taxonomy->required) || !$taxonomy->required )
//		echo "<option value=''>(" . sprintf(__('no %s'), $taxonomy->labels->name) . ")</option>";
//
//	echo wp_terms_checklist($post->ID, array( 'taxonomy' => $taxonomy->name, 'selected_cats' => $post->ID, 'walker' => new Walker_Taxonomy_Select) );
//	echo "</select>";
//}
class Walker_Taxonomy_Select extends Walker {
	var $tree_type = 'taxonomy_select';
	var $db_fields = array ('parent' => 'parent', 'id' => 'term_id');

	function start_lvl(&$output, $depth, $args) {
	}

	function end_lvl(&$output, $depth, $args) {
	}

	function start_el(&$output, $term, $depth, $args) {
		$indent = str_repeat("\t", $depth);

		extract($args);
		if ( $taxonomy == 'category' )
			$name = 'post_category';
		else
			$name = "tax_input[{$taxonomy}]";

		$class = '';
		$output .= "\n<option id='{$taxonomy}-{$term->term_id}'$class value='{$term->name}'" . selected( in_array( $term->term_id, $selected_cats ), true, false ) . disabled( empty( $args['disabled'] ), false, false ) . ' /> ' . $indent . esc_html( apply_filters('the_category', $term->name )) . '</option>';
	}

	function end_el(&$output, $category, $depth, $args) {
	}
}

////custom feeds
function custom_rss($content) {
	if( get_query_var( 'post_type' )=='news'):
		global $post;
        $content = '<p><a href="'.get_field('url').'">'.get_the_title().'</a> <br/> Source: '.get_field('source').'</p><p>'.get_field('excerpt').'</p>';
	endif;
	return $content;
}
add_filter('the_excerpt_rss','custom_rss');
add_filter('the_content_rss','custom_rss');


//hack gravity forms to properly save to wp_postmeta
add_action('gform_post_submission', 'gravity_add_post_meta',10,2);

function gravity_add_post_meta($entry,$form){
	global $wpdb;
	if(!class_exists('Acf'))
		return;
	$post_id = $entry['post_id'];
	foreach($form['fields'] as $field):
		$key = $field['adminLabel'];
		$i = (int) $field['id'];
		$value = $entry[$i];
		add_post_meta($post_id, $key, $value);
	endforeach;
	return;
}

// allow gravity forms to not strip certain html tags
add_filter("gform_allowable_tags", "allow_basic_tags");
function allow_basic_tags($allowable_tags){
	return '<p><a><strong><u><em><ul><ol><li><span><div><blockquote><h1><h2><h3><h4><h5><h6>';
}

// Add custom taxonomies and custom post types counts to dashboard
add_action( 'dashboard_glance_items', 'my_add_cpt_to_dashboard' );

function my_add_cpt_to_dashboard() {
  $showTaxonomies = 1;
  // Custom taxonomies counts
  if ($showTaxonomies) {
    $taxonomies = get_taxonomies( array( '_builtin' => false ), 'objects' );
    foreach ( $taxonomies as $taxonomy ) {
      $num_terms  = wp_count_terms( $taxonomy->name );
      $num = number_format_i18n( $num_terms );
      $text = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name, $num_terms );
      $associated_post_type = $taxonomy->object_type;
      if ( current_user_can( 'manage_categories' ) ) {
        $output = '<a href="edit-tags.php?taxonomy=' . $taxonomy->name . '&post_type=' . $associated_post_type[0] . '">' . $num . ' ' . $text .'</a>';
      }
      echo '<li class="taxonomy-count">' . $output . ' </li>';
    }
  }
  // Custom post types counts
  $post_types = get_post_types( array( '_builtin' => false ), 'objects' );
  foreach ( $post_types as $post_type ) {
    if($post_type->show_in_menu===false) {
      continue;
    }
    $num_posts = wp_count_posts( $post_type->name );
    $num = number_format_i18n( $num_posts->publish );
    $text = _n( $post_type->labels->singular_name, $post_type->labels->name, $num_posts->publish );
    if ( current_user_can( 'edit_posts' ) ) {
        $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
    }
    // pending items count
    // if ( $num_posts->pending > 0 ) {
    //     $num = number_format_i18n( $num_posts->pending );
    //     $text = _n( $post_type->labels->singular_name . ' pending', $post_type->labels->name . ' pending', $num_posts->pending );
    //     if ( current_user_can( 'edit_posts' ) ) {
    //         $output .= '<a class="waiting" href="edit.php?post_status=pending&post_type=' . $post_type->name . '">' . $num . ' pending</a>';
    //     }
    // }
    echo '<li class="page-count ' . $post_type->name . '-count">' . $output . '</td>';
  }
}



function format_articles($data){
	global $info;
	$fields = get_fields($data->ID);
	$pg = explode("-", $fields['page_number'], 2);
	$firstpage = $pg[0];
	$permalink = get_permalink($data->ID);
		?>
			<div class="item">
				<h6 class="title"><a href="<?=$permalink;?>"><?=$data->post_title?></a> <span class="author"><?=$fields['primary_author']?></span></h6>
				<div class="actions">
					<a href="<?=$permalink;?>">More</a>
				</div>
			</div>
		<?php
}


// utility admin pages
add_action('admin_menu' , 'dmh_enable_utils'); 
 
function dmh_enable_utils() {
  add_submenu_page( 'tools.php', 'DMH Utilities', 'DMH Utilities', 'update_core', 'dmh_utilities', 'dmh_utilities');
}
function dmh_utilities(){
  if ( !current_user_can( 'update_core' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  } 
?>
<div class="wrap">
  
  <div id="icon-tools" class="icon32"></div>
  <h2>DMH Utilities</h2>
  
  <div id="poststuff">
  
    <div id="post-body" class="metabox-holder ">
    
      <!-- main content -->
      <div id="post-body-content">
        
          
          <div class="postbox">
          
            <h3><span>Options</span></h3>
            <div class="inside">
                  <table class="widefat" cellspacing="0">
                    <tr>
                      <td class="row-title"><a href="tools.php?page=dmh_utilities&amp;action=update_dates">Update Date Fields</a></td>
                    </tr>
                    <tr>
                      <td class="row-title"><a href="tools.php?page=dmh_utilities&amp;action=update_resources">Update Resources</a></td>
                    </tr>
                  </table>
            </div> <!-- .inside -->
          
          </div> <!-- .postbox -->
<?php if($_GET['action'] == 'update_dates'): ?>
          <div class="postbox">
            <h3><span>Updating Dates</span></h3>
            <div class="inside">
<?php
    $query = null;
    $query = array(
      'post_type' => 'events',
      'post_status' => 'publish',
      'posts_per_page' => -1
    );
    $posts = get_posts($query);
    
    $i = 0;
    $date_format = 'Ymd';
    foreach($posts as $post) : setup_postdata($post);
      
      $start_date = get_post_meta($post->ID, 'start_date', true);
      $end_date = get_post_meta($post->ID, 'end_date', true);
      $abs_date = get_post_meta($post->ID, 'abstracts_due_date', true);
      $clean_start = date($date_format,strtotime($start_date));

      if($end_date == false):
        $end_date = $clean_start;
      else:
        $clean_end = date($date_format,strtotime($end_date));
      endif;

      if($abs_date != false):
        $clean_abs = date($date_format,strtotime($abs_date));
      endif;
      update_field('field_4f205d702e43a', $clean_start, $post->ID);
      if($clean_end) update_field('field_4f205d702e926', $clean_end, $post->ID);
      if($clean_abs) update_field('field_50cbcb052b2d4', $clean_abs, $post->ID);
      echo "cleaning ". $post->post_title;
      var_dump($clean_start);
      var_dump($clean_end);
      var_dump($clean_abs);
      echo "<hr>";

      $i++;
    endforeach;
    echo "<h2>".$i." dates updated</h2>";
?>
            </div> <!-- .inside -->
          </div> <!-- .postbox -->
<?php elseif($_GET['action'] == 'update_resources'): ?>
          <div class="postbox">
            <h3><span>Updating Resources</span></h3>
            <div class="inside">
<?php
    $query = null;
    $query = array(
      'post_type' => 'links',
      'post_status' => 'publish',
      'posts_per_page' => -1
    );
    $posts = get_posts($query);
    
    $i = 0;
    
    foreach($posts as $post) : setup_postdata($post);
      $url = get_post_meta($post->ID, 'url', true);
      $soruce = get_post_meta($post->ID, 'source', true);
      $excerpt = get_post_meta($post->ID, 'excerpt', true);
      echo "cleaning ". $post->post_title;

      update_field('field_4f1f440e2cfa5', $url, $post->ID);
      update_field('field_4f1f440e2d364', $source, $post->ID);
      update_field('field_4f1f440e2d631', $excerpt, $post->ID);
      var_dump($url);
      // var_dump($clean_end);
      // var_dump($clean_abs);
      echo "<hr>";

      $i++;
    endforeach;
    echo "<h2>".$i." dates updated</h2>";
?>
            </div> <!-- .inside -->
          </div> <!-- .postbox -->
<?php endif; ?>         
        
      </div> <!-- post-body-content -->
      
      
    </div> <!-- #post-body .metabox-holder -->
    
    <br class="clear">
  </div> <!-- #poststuff -->
  
</div> <!-- .wrap -->
<?php  
}