<?php
// Post Types and taxonomies
function my_post_types(){
	
  // Journal Issues
  register_post_type('issues', 
    array(	
      'label' => 'Journals',
      'description' => 'Issues of AJOB',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'page',
      'hierarchical' => false,
      'publicly_queryable' => true,
      'rewrite' => array('slug' => 'journals', 'with_front' => true),
//      'rewrite' => array('slug' => 'journals'),
      'query_var' => true,
      'has_archive' => true,
      'supports' => array('title'),
      'taxonomies' => array('editions'),
      'labels' => array (
              'name' => 'Journals',
              'singular_name' => 'Journal',
              'menu_name' => 'Journals',
              'add_new' => 'Add New Issue',
              'add_new_item' => 'Add New Issue',
              'edit' => 'Edit',
              'edit_item' => 'Edit Issue',
              'new_item' => 'New Issue',
              'view' => 'View Issue',
              'view_item' => 'View Issue',
              'search_items' => 'Search Issues',
              'not_found' => 'No Journals Found',
              'not_found_in_trash' => 'No Journals Found in Trash',
              'parent' => 'Parent Journal',
              'all_items' => 'All Journals'
      )
    ) 
  );

  // Articles
  register_post_type('Articles', 
    array(
      'label' => 'Articles',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
//      'rewrite' => array('slug' => '/journal/%editions%/%volume%/%issue%/%a%', 'with_front' => false),
	  'rewrite' => array('slug' => 'articles','with_front'=>false),
      'query_var' => true,
      'has_archive' => true,
      'supports' => array('title','comments'),
      'labels' => array (
        'name' => 'Articles',
        'singular_name' => 'Article',
        'menu_name' => 'Articles',
        'add_new' => 'Add article',
        'add_new_item' => 'Add New article',
        'edit' => 'Edit',
        'edit_item' => 'Edit article',
        'new_item' => 'New article',
        'view' => 'View article',
        'view_item' => 'View article',
        'search_items' => 'Search Articles',
        'not_found' => 'No Articles Found',
        'not_found_in_trash' => 'No Articles Found in Trash',
        'parent' => 'Parent article',
        'all_items' => 'All Articles'
      ),
      'taxonomies' => array('category', 'post_tag')
    ) 
  );
  
  // Authors
//  register_post_type('authors', 
//    array(
//      'label' => 'Authors',
//      'description' => '',
//      'public' => true,
//      'show_ui' => true,
//      'show_in_menu' => true,
//      'capability_type' => 'post',
//      'hierarchical' => false,
//      'rewrite' => array('slug' => 'journal/authors'),
//      'query_var' => true,
//      'menu_position' => 30,
//      'supports' => array('title',),
//      'labels' => array (
//        'name' => 'Authors',
//        'singular_name' => 'Author',
//        'menu_name' => 'Authors',
//        'add_new' => 'Add Author',
//        'add_new_item' => 'Add New Author',
//        'edit' => 'Edit',
//        'edit_item' => 'Edit Author',
//        'new_item' => 'New Author',
//        'view' => 'View Author',
//        'view_item' => 'View Author',
//        'search_items' => 'Search Authors',
//        'not_found' => 'No Authors Found',
//        'not_found_in_trash' => 'No Authors Found in Trash',
//        'parent' => 'Parent Author',
//        'all_items' => 'All Authors'
//      )
//    ) 
//  );
  
  // Events
  register_post_type('events', 
    array(
      'label' => 'Events',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
	  'has_archive' => true,
      'rewrite' => array('slug' => 'events'),
      'query_var' => true,
      'supports' => array('title','editor','excerpt','custom-fields','revisions'),
      'labels' => array (
        'name' => 'Events',
        'singular_name' => 'Events',
        'menu_name' => 'Events',
        'add_new' => 'Add New Event',
        'add_new_item' => 'Add New Event',
        'edit' => 'Edit',
        'edit_item' => 'Edit Event',
        'new_item' => 'New Event',
        'view' => 'View Event',
        'view_item' => 'View Event',
        'search_items' => 'Search Events',
        'not_found' => 'No Events Found',
        'not_found_in_trash' => 'No Events found in Trash',
        'parent' => 'Parent Events',
        'all_items' => 'All Events'
      )
    ) 
  );
  
  // News
  register_post_type('news', 
    array(
      'label' => 'News',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => array('slug' => 'news'),
      'query_var' => true,
      'has_archive' => true,
      'supports' => array('title',),
      'taxonomies' => array('category','post_tag',),
      'labels' => array (
        'name' => 'News',
        'singular_name' => 'News',
        'menu_name' => 'News',
        'add_new' => 'Add News',
        'add_new_item' => 'Add New News Item',
        'edit' => 'Edit',
        'edit_item' => 'Edit Item',
        'new_item' => 'New Item',
        'view' => 'View',
        'view_item' => 'View News Item',
        'search_items' => 'Search News',
        'not_found' => 'No News Found',
        'not_found_in_trash' => 'No News found in Trash',
        'parent' => 'Parent News',
        'all_items' => 'All News'
      )
    )
  );
  
  // Links
  register_post_type('links', 
    array(
      'label' => 'Links',
      'description' => '',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => array('slug' => 'resources'),
      'query_var' => true,
      'has_archive' => true,
      'supports' => array('title',),
      'taxonomies' => array('category','post_tag',),
      'labels' => array (
        'name' => 'Resources',
        'singular_name' => 'Resource',
        'menu_name' => 'Resources',
        'add_new' => 'Add Resource',
        'add_new_item' => 'Add New Resource',
        'edit' => 'Edit',
        'edit_item' => 'Edit Resource',
        'new_item' => 'New Resource',
        'view' => 'View',
        'view_item' => 'View Resource',
        'search_items' => 'Search Resources',
        'not_found' => 'No Resources Found',
        'not_found_in_trash' => 'No Resources found in Trash',
        'parent' => 'Parent Resource',
        'all_items' => 'All Resources'
      )
    )
  );
  
// jobs
  register_post_type('jobs', 
    array(
      'label' => 'Jobs',
      'description' => 'Jobs in Bioethics',
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'capability_type' => 'post',
      'hierarchical' => false,
      'rewrite' => array('slug' => 'jobs'),
      'query_var' => true,
      'has_archive' => true,
      'supports' => array('title',),
      'labels' => array (
        'name' => 'Jobs',
        'singular_name' => 'Job',
        'menu_name' => 'Jobs',
        'add_new' => 'Add Job',
        'add_new_item' => 'Add New Job',
        'edit' => 'Edit',
        'edit_item' => 'Edit Job',
        'new_item' => 'New Job',
        'view' => 'View',
        'view_item' => 'View Job',
        'search_items' => 'Search Jobs',
        'not_found' => 'No Jobs Found',
        'not_found_in_trash' => 'No Jobs found in Trash',
        'parent' => 'Parent Jobs',
        'all_items' => 'All Jobs'
      )
    )
  );
  
// books
  // register_post_type('books', 
  //   array(
  //     'label' => 'Books',
  //     'description' => 'Books in Bioethics',
  //     'public' => true,
  //     'show_ui' => true,
  //     'show_in_menu' => true,
  //     'capability_type' => 'post',
  //     'hierarchical' => false,
  //     'rewrite' => array('slug' => 'books'),
  //     'query_var' => true,
  //     'has_archive' => true,
  //     'supports' => array('title',),
  //     'taxonomies' => array('category'),
  //     'labels' => array (
  //       'name' => 'Books',
  //       'singular_name' => 'Book',
  //       'menu_name' => 'Books',
  //       'add_new' => 'Add Book',
  //       'add_new_item' => 'Add New Book',
  //       'edit' => 'Edit',
  //       'edit_item' => 'Edit Book',
  //       'new_item' => 'New Book',
  //       'view' => 'View',
  //       'view_item' => 'View Book',
  //       'search_items' => 'Search Books',
  //       'not_found' => 'No Books Found',
  //       'not_found_in_trash' => 'No Books found in Trash',
  //       'parent' => 'Parent Books',
  //       'all_items' => 'All Books'
  //     )
  //   )
  // );
  
  // Editions Taxonomy
  register_taxonomy('editions',
    array (0 => 'issues'),
    array( 
      'hierarchical' => true, 
      'labels' => array(
        'name' => 'Journal Editions',
        'singular_name' => 'Journal Edition',
        'add_new_item' => 'Add New Edition',
        'edit_item' => 'Edit Edition',
        'update_item' => 'Update Edition',
        'search_items' => 'Search Editions'
      ),
      'show_ui' => true, 
      'query_var' => true,
      'rewrite' => array('slug' => 'editions','with_front' => true),
      'singular_label' => 'Journal',
      'single_value'  => true,
      'required'  => true
    ) 
  );
  
  // Article Types
  register_taxonomy('article_types',
    array (0 => 'articles'),
    array( 
      'hierarchical' => true, 
      'labels' => array(
        'name' => 'Article Types',
        'singular_name' => 'Article Type',
        'add_new_item' => 'Add New Article Type',
        'edit_item' => 'Edit Article Type',
        'update_item' => 'Update Article Type',
        'search_items' => 'Search Article Types'
      ),
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => array('slug' => 'article_types'),
      'singular_label' => 'Article',
      'single_value'  => true,
      'required'  => true
    ) 
  );
  
}
add_action('init', 'my_post_types');

// Publishers
register_taxonomy('Publishers',
	array (0 => 'books'),
	array( 
		'hierarchical' => false, 
		'label' => 'Publishers',
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'publisher'),
		'singular_label' => 'publisher'
	) 
);

//
//add_filter('post_link', 'journal_permalink', 10, 3);
//add_filter('post_type_link', 'journal_permalink', 10, 3);
//function journal_permalink($permalink, $post_id, $leavename) {
//	if (strpos($permalink, '%editions%') === FALSE) return $permalink;
//        // Get post
//        $post = get_post($post_id);
//        if (!$post) return $permalink;
//		
//		//if is article, get info from related journal
//		if(strstr($permalink,'%a%')):
//			$myid = get_field('ajob_issue',$post->ID);
//			$myid = $myid->ID;
//			$permalink = str_replace('/%a%', '', $permalink);
//		//if is journal, get info from self
//		else:
//			$myid = $post->ID;
//			//remove post name
//			$permalink = str_replace('/'.$post->post_name, '', $permalink);
//		endif;
//		
//		// Get journal 
//		$terms = wp_get_object_terms($myid, 'editions');	
//		if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0]))
//			$taxonomy_slug = $terms[0]->slug;
//		else $taxonomy_slug = 'ajob';
//		$permalink = str_replace('%editions%', $taxonomy_slug, $permalink);		
//
//		if(strstr($permalink,'%volume%')):
//			// Get journal volume
//			$volume = get_field('volume',$myid);
//			if($volume != false)
//				$permalink = str_replace('%volume%', $volume, $permalink);
//		endif;
//		if(strstr($permalink,'%issue%')):
//			// Get journal volume
//			$issue = get_field('number',$myid);
//			if($issue != false)
//				$permalink = str_replace('%issue%', $issue, $permalink);
//		endif;
//	return $permalink;
//}
