// Copyright 2012 Diamond Merckens Hogan. All Rights Reserved.
// Bioethics.net

/**
* @fileoverview Contains the JavaScript used by the Bioethics site. This file depends on
* the jQuery 1.7 library.
* @author keshleman@diamondmerckenshogan.com (Kevin Eshleman)
*/

function SlideShow(container, options) {
	this.options = $.extend({
		'animated': false,
		'fadeInTime': 300,
		'fadeOutTime': 300,
		'timerDelay': 10000
	}, options);
	this.slides = container.find('.slide');
	this.slideLinks = container.find('.slide-link');
	this.timer = null;
	this.showSlide(0);

	// Closure for handling slide link clicks.
	var thisObject = this;
	$('.slide-link').click(function(e) {
		e.preventDefault();
		thisObject.stopTimer();
		thisObject.showSlide($(this).data('id'));
	});
}

SlideShow.prototype.onTimerFire = function(sender) {
	sender.showNextSlide();
	sender.resetTimer();
};

SlideShow.prototype.stopTimer = function() {
	if (this.timer) {
		clearInterval(this.timer);
		this.timer = null;
	}
};

SlideShow.prototype.startTimer = function() {
	var thisObject = this;
	this.timer = setInterval(function() {
		thisObject.showNextSlide();
	}, this.options.timerDelay);
};

SlideShow.prototype.showNextSlide = function() {
	if (this.currentIndex === this.slides.length - 1)
		this.showSlide(0);
	else
		this.showSlide(this.currentIndex + 1);
};

SlideShow.prototype.showPrevSlide = function() {
	if (this.currentIndex === 0)
		this.showSlide(this.slides.length - 1);
	else
		this.showSlide(this.currentIndex - 1);
};

SlideShow.prototype.showSlide = function(index) {
	// Check to see if the slide is already being shown.
	if (this.currentIndex === index)
		return;

	// Avoid hiding a slide if one is not being shown.
	var oldIndex;
	if (this.currentIndex !== undefined)
		oldIndex = this.currentIndex;

	// Hide the currently shown slide and show the slide with the given index.
	$(this.slideLinks[this.currentIndex]).removeClass('active');
	$(this.slideLinks[index]).addClass('active');
	if (this.options.animated === true) {
		if (oldIndex !== undefined)
			$(this.slides[oldIndex]).fadeOut(this.options.fadeOutTime);
		$(this.slides[index]).fadeIn(this.options.fadeInTime);
	} else {
		if (oldIndex !== undefined)
			$(this.slides[oldIndex]).hide();
		$(this.slides[index]).show();
	}
	this.currentIndex = index;
};


var Bioethics = {
	journalSlideShow: null,

	// Initializes the Bioethics web page.
	init: function() {
		// Set the media url
		jQuery.media_url = jQuery('link#media-url').attr('href');

		Bioethics.journalSlideShow = new SlideShow($('#journal-feature'), {
			'animated': true,
			'fadeInTime': 250,
			'fadeOutTime': 250,
			'timerDelay': 5000
		});
		Bioethics.journalSlideShow.startTimer();

		// Handle when journal link item is clicked.
		$("#primary-nav li.menu-item a:contains('Journal'), #nav-journals div.close").click(function(e) {
			Bioethics.toggleJournalNav(e);
		});

		// split ul into two separate lists
		$(".split > ul").each(function() {
			var $ul = $(this).addClass("column01"), // Let original be first column
			$lis = $ul.children(), // Find all children `li` elements
			mid = Math.floor($lis.length / 2), // Calculate where to split
			$newCol = $('<ul />', {
				"class": "column02"
			}).insertAfter($ul); // Create new column and add after original
		
			$lis.each(function(i) {
				if (i >= mid) $(this).appendTo($newCol); // Move `li` elements with index greater than middle
			});
		});
		// add image to lists that can be expanded
		$("#hot-topics ul li:has(ul)").each(function(){
			var el = $('<img/>').
				attr('src', $.media_url+'/images/btn-green-down.png').
				click(function(){
					$(this).next('ul').slideToggle(500,'swing');
				});			
			$(this).children('ul').before(el);
			
		});
		// Limit width of images on blog archive/list
		$("#main-content img, body.archive div.type-post img").each(function() {
			var width = $(this).width();
			var height = $(this).height();
			var max = 250;
			//Max-width substitution (works for all browsers)
			if (width > max || height > max) {
				var longest = (width > height) ? 'w' : 'h';
				if(longest=='w'){
					$(this).css({
						"width": max+"px",
						"height" : "auto"
					}).removeAttr('width height');
				}else{
					$(this).css({
						"width": "auto",
						"height" : max+'px'
					}).removeAttr('width height');
				}
			}
			// remove movabletype styles
			
			if($(this).is('img.mt-image-right, img.mt-image-left, .mt-enclosure-image img')){
				console.log(this);
				$(this).prependTo($(this).closest('.entry')).
				css({
					'margin':'',
					'float':''
				}).
				addClass('feature-img');
			}
		});

		// Setup the template Child-page-list items to slide the item text up and down when clicked.
		$('.list-item h3.list-item-title,.list-item h4.list-item-title').click(function() {
			$('.list-item-text[data-id="' + $(this).attr('data-id') + '"]').slideToggle(400);
		});
		
		// for pages with wysiwyg forms
		if(typeof window.tinyMCE == 'object'){
			tinyMCE.init({
				mode : "exact",
				elements : "input_1_12,input_2_13", // textarea ids
				plugins: "paste",
				theme : "advanced",
				height:"250",
				width:"350",

				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,|,bullist,numlist,|,link,unlink,|,justifyleft,justifycenter,justifyright|cut,copy,paste,pastetext,pasteword,|,code",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3 : "",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true
			});
		}
	},

	toggleJournalNav: function(e){
		e.preventDefault();
		$('#nav-journals').slideToggle(500,'swing',function(){
			$('li#menu-item-24331, body').toggleClass('journal-active');
		}); 
	}
};

// Called after the document loads to initilize the page.
$(document).ready(function($) {
	Bioethics.init();
});