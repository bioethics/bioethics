<?php
//get pagination page
global $paged;
$type = get_post_type();

if($type == 'news'):
	$per_page = 30;
	$today = date('Y/m/d');
	$vars['posts_per_page'] = $per_page;
	$vars['paged']			= $paged; // enable pagination on custom post types
	$vars['post_type']		= $type;
endif;

if(isset ($vars)):
	$posts = query_posts($vars);
endif;

if ( have_posts() ) : 
	$lastmonth = null;
	$i = 1;
	$cols = 3;
	// var_dump($posts);
	while ( have_posts() ) : the_post(); 
		 
		switch($type):
			
			
			
			
			
			
			//EVENTS
			case 'events':
				$start = get_field('start_date');
				$end = get_field('end_date');
				$thismonth = date('F Y',strtotime($start));				
				if($lastmonth==null || $lastmonth != $thismonth):
					if($i != 1 && $lastmonth != null) echo '</div> <!-- /.row -->';
					echo '<h2 class="month">'.$thismonth.'</h2>';
					$i = 1;
				endif;
				
				if($i==1) echo '<div class="row">';
				?>
	<div id="event-<?php the_ID(); ?>" <?php post_class('list-post col-'.$i); ?>>
			<h3 class="date">
				<?php 
				echo date('l, F jS Y',strtotime($start));
				if($end && $end != $start) echo " - ".date('l, F jS Y',strtotime($end));
				?>
			</h3>
			<h4 class="title"><a href="<?php the_permalink() ?>" title="View info for <?php the_title() ?>"><?php the_title();?></a></h4>
			<h5 class="location"><?php 
				$location = array();
				$city =	get_field('city');
				if($city && $city !='') $location[] = $city;
				$state = get_field('state');
				if($state && $state !='') $location[] = $state;
				$province = get_field('province');
				if($province && $province!='') $location[] = $province;
				$country = get_field('country');
				if($country && $country !='') $location[] = $country;

				echo implode(', ',$location);
			?></h5>
			<div class="description">
				<?php	the_advanced_excerpt('length=15&allowed_tags=a&finish_sentence=0'); ?>
			</div>
			<p class="more"><a href="<?php the_permalink() ?>" title="View info for <?php the_title() ?>">Details</a></p>
		<?php	
				$lastmonth = $thismonth;	
				break;






			//NEWS 
			case 'news':		
				if($i==1) echo '<div class="row">';
				?>
	<div id="news-<?php the_ID(); ?>" <?php post_class('list-post col-'.$i); ?>>
				<h3 class="date"><?php echo get_the_date();?></h3>
				<h4 class="title"><a href="<?php the_field('url') ?>" title="<?php the_field('source')?>: <?php the_title() ?>"><?php the_title();?></a></h4>
				<h5><?php the_field('source')?></h5>
				<div class="description">
					<?php	the_field('excerpt')?>
				</div>
			<?php
				break;
		endswitch; 
		edit_post_link('Edit this', ' <p class="edit-link">', '</p>');
		if($i==$cols):
			$i = 1;
			echo '</div> <!-- /.row -->';
		else:
			$i++;
		endif;
		?>	
	</div><!--end list-post-->
	<?php 

	endwhile; /* rewind or continue if all posts have been fetched */ 
	?>
	<div id="pagination">
	<?php if(is_single()): ?>
		<span class="nav-new">
			<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
		</span>
		<span class="nav-old">
			<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
		</span>
	<?php else: ?>
		<span class="nav-old">
			<?php next_posts_link('&larr; Older entries '); ?>
		</span>
		<span class="nav-new">
			<?php previous_posts_link('Newer entries &rarr;'); ?>
		</span>
	<?php endif; ?>
	</div><!-- /#pagination-->
<?php else : ?>
<h1>No Posts</h1>	
<?php endif; ?>
