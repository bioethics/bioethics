<?php 
/**
 * Template Name: Submit a Job
 *
 */

require_once('header.php'); ?>
<div id="page-content" class="section">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<h2 class="title"> Bioethics Jobs. <span><a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs" id="subnav_current">Current Bioethics Jobs</a> | <a href="<?php bloginfo('wpurl')?>/submit_job" id="subnav-submit">Submit a Job</a></span></h2>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
			<div class="entry">
				<?php the_content(); ?>
				<?php edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>	
				<div class="social">
					<?php if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
				</div>
			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; ?>
</div>
<?php 
endif;
require_once('footer.php');
