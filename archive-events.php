<?php require_once('header.php'); ?>
<div id="list-content">
	<?php 
	if ( have_posts() ) : ?>
		<h2 class="title"> Bioethics Events. <span><a href="<?php bloginfo('wpurl')?>/events" title="Current Bioethics Events" id="subnav_current">Current Bioethics Events</a> | <a href="<?php bloginfo('wpurl')?>/submit_event" id="subnav-submit">Submit an Event</a></span></h2>
		<?php get_template_part( 'loop','short' ); ?>
	<?php else : ?>
		<p>No posts found.</p>
	<?php endif; 
?>
</div><!--end content-->
<?php require_once('footer.php');
