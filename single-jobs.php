<?php require_once('header.php'); ?>
<div id="page-content" class="section">
	<h2 class="title"> Bioethics Jobs. <span><a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs" id="subnav_current">Current Bioethics Jobs</a>  | <a href="<?php bloginfo('wpurl')?>/submit_job" id="subnav_submit">Submit a Job</a></span></h2>
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); 
		$company = get_field('institution_name');
		$deadline = get_field('application_deadline');
		$deadline_text = get_field('application_deadline_text');
		$description = get_field('long_description');
		$city =	get_field('city');
		$state = get_field('state');
		$province = get_field('province');
		$country = get_field('country');
		
		$name = get_field('name');
		$tele = get_field('telephone');
		$email = get_field('email'); 
	?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
			<div class="post-header">
				<h5 class="date">Posted: <?php 
					echo get_the_date('l, F jS, Y');
				?></h5>
				<h3 class="post-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php the_title(); ?>
					</a>
				</h3>

			</div><!--end post-header-->
			<div class="entry">
			<?php if($deadline): ?>
				<div class="row">
					<h5 class="job_label">Deadline:</h5>
					<div class="job_info"><?= ($deadline_text!='') ? $deadline_text : date('l, F jS, Y',  strtotime($deadline))?></div>
				</div>
			<?php endif; ?>
			<div class="row">
				<h5 class="job_label">Location:</h5>
				<div class="job_info">
					<h6><?=$company?></h6>
					<p>	<?php 
				$location = array();
				if($city && $city !='') $location[] = $city;
				if($state && $state !='') $location[] = $state;
				if($province && $province!='') $location[] = $province;
				if($country && $country !='') $location[] = $country;
				echo implode(', ',$location);
					?></p>
				</div>
			</div>
		<?php
		if($name): ?>
			<div class="row">
				<h5 class="job_label">Contact:</h5>
				<div class="job_info contact">
					<?=$name;?></br>
					<?=$tele;?></br>
					<a href="mailto:<?=$email?>"><?=$email?></a>
				</div>
			</div>
		<?php endif; ?>
		
			<div class="row last">
				<h5 class="job_label">Details:</h5>
				<div class="job_info">
					<?= $description ?>	
				</div>
			</div>
				<div class="meta">
					<p>
<?php
$categories_list = get_the_category_list( ', ');
$tag_list = get_the_tag_list( '', ', ');
if ( '' != $tag_list ) {
	$utility_text = 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				} elseif ( '' != $categories_list ) {
					$utility_text = 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				} else {
					$utility_text = 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				}

				printf(
					$utility_text,
					$categories_list,
					$tag_list,
					esc_url( get_permalink() ),
					the_title_attribute( 'echo=0' )
				);
				edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>
					</p>
				</div>
				<div class="social">
					<?php if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
				</div>


			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; ?>
	
	<div id="pagination">
	<?php if(is_single()): ?>
		<span class="nav-new">
			<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
		</span>
		<span class="nav-old">
			<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
		</span>
	<?php endif; ?>
	</div><!-- /#pagination-->
<?php else : ?>
	<h1>No Posts!</h1>	
<?php endif; ?>

</div>
<?php
require_once('footer.php');