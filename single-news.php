<?php require_once('header.php'); ?>
<div id="main-content" class="section">
  <h2 class="rss"><a href="/news/feed" title="Bioethics News RSS Feed"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="News RSS"></a><a href="<?php bloginfo('url')?>/blog"> Bioethics News.</a></h2>
	<?php 
		// separate results by type
	while ( have_posts() ) : the_post(); ?>
	
  
  <div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
			<div class="post-header">
				<h5 class="date"><?php echo  get_the_date(); ?></h5>
				<h3 class="post-title">
					<a href="<?php the_field('url') ?>" rel="bookmark">
						<?php the_title(); ?>
					</a>
				</h3>

			</div><!--end post-header-->
			<div class="entry">
				
				<h4><a href="<?=the_field('url')?>" title="<?php the_title(); ?>" target="_blank">External Link</a> - Source: <?=the_field('source')?></h4>
				<p><?=the_field('excerpt')?></p>
				<div class="meta">
					<p>
<?php
$categories_list = get_the_category_list( ', ');
$tag_list = get_the_tag_list( '', ', ');
if ( '' != $tag_list ) {
	$utility_text = 'This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				} elseif ( '' != $categories_list ) {
					$utility_text = 'This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				} else {
					$utility_text = 'Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.';
				}

				printf(
					$utility_text,
					$categories_list,
					$tag_list,
					esc_url( get_permalink() ),
					the_title_attribute( 'echo=0' )
				);
				edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>
					</p>
				</div>
				<div class="social">
					<?php if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
				</div>


			</div><!--end entry-->
		</div><!--end post-->
  
  
  
  

	
	<?php
	endwhile;
	?>
</div>

	<div id="sidebar-blog" class="sidebar section">
		<ul>
			<?php dynamic_sidebar('blog-sidebar'); ?>
		</ul>
	</div>
<?php
require_once('footer.php');


		
?>