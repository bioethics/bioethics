
						<div class="banner-ad">
							<!-- Bioethics banner footer [async] -->
							<script type="text/javascript">if (!window.AdButler){(function(){var s = document.createElement("script"); s.async = true; s.type = "text/javascript";s.src = 'http://ab164087.adbutler-tachyon.com/app.js';var n = document.getElementsByTagName("script")[0]; n.parentNode.insertBefore(s, n);}());}</script>
							<script type="text/javascript">
							var AdButler = AdButler || {}; AdButler.ads = AdButler.ads || [];
							var abkw = window.abkw || '';
							var plc182357 = window.plc182357 || 0;
							document.write('<div id="placement_182357_'+plc182357+'"></div>');
							AdButler.ads.push({handler: function(opt){ AdButler.register(164087, 182357, [728,90], 'placement_182357_'+opt.place, opt); }, opt: { place: plc182357++, keywords: abkw, domain: 'ab164087.adbutler-tachyon.com' }});
							</script>				
						</div>
					</div><!--end .container -->
				</div><!--end .content -->
			</div><!--end #wrapper -->
		</div><!--end #global-wrap -->

		<div id="footer">
			<div class="container">

				<div class="footer-boxes">
					<ul>
						<li id="footer-signup">
							<h3>Sign up for <a href="/newsletter/">Bioethics Today</a></h3>
							<a id="footer-subscribe-button" href="/newsletter/">Sign Up</a>
							<!-- <a id="footer-subscribe-archive" href="/newsletter/">Archives</a> -->
						</li>

						<?php 
							$footer_events = get_transient( 'footer_events');
							if(false === $footer_events):
								$today = date('Y/m/d');
								$vars['order'] = 'ASC';
								$vars['posts_per_page'] = 2;
								$vars['post_type']		= 'events';
								$vars['meta_type'] = 'DATE';
								$vars['meta_key'] = 'start_date';
								$vars['orderby'] = 'meta_value_num';
								$vars['meta_query'] = array(
									array(
										'key' => 'end_date',
										'compare' => '>=',
										'value'	=> $today,
										'type'	=> 'DATE'
									)
								);
								$footer_events = new WP_Query($vars);
								set_transient('footer_events',$footer_events, YEAR_IN_SECONDS );
							endif;
							$i = 0;
							if ( $footer_events->have_posts() ) : 
								while ( $footer_events->have_posts() ) : $footer_events->the_post(); 
									$start = get_field('start_date');
									$end = get_field('end_date');
						?>
						<li class="footer-events list"  id="footer-news-<?=$i?>">
							<h3><a href="<?php bloginfo('wpurl')?>/events" title="Upcoming Bioethics Events">Upcoming events.</a></h3>
							<div class="footer-news">
							<h5 class="date"><?php
							echo date('F jS',strtotime($start));
							if($end && $end != $start) echo " - ".date('F jS',strtotime($end));
							?></h5>
							<h6 class="title"><a href="<?php the_permalink()?>"><?php the_title() ?></a></h6>
							</div>
						</li>
						<?php		
								$i++;
								endwhile;
							endif;
						?>
							

						<li id="footer-jobs">
							<h3><a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs">BIOETHICS JOBS.</a></h3><br />
							<div id="job-count"><a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs"><?php 
							echo get_current_jobs(true);?></a></div>
							<?php // print_r($jobs);?>
							<h6><a href="<?php bloginfo('wpurl')?>/jobs" title="Current Bioethics Jobs">Positions currently listed</a></h6>
						</li>
						<!-- li id="footer-books">
							<img src="<?php echo get_template_directory_uri(); ?>/images/book-image2.png" width="60" height="89" />
							<h3>Selected Books in Ethics.</h3>
							<p><a href="#">Bioethics and the Brain</a><br />
								By Walter Glannon</p>
						</li -->
					</ul>
				</div>
				<?php
				wp_nav_menu(
						array(
							'menu' => 'footer-links',
							'container' => 'div',
							'container_class' => 'footer-links',
						)
				);
				?>

			</div><!--end #footer .container -->
		</div><!--end #footer -->
		<?php wp_footer();
		if (!WP_DEBUG):
			?>
			<script type="text/javascript">
				var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
				document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));

			</script>
			<script type="text/javascript">
				try {
					var pageTracker = _gat._getTracker("UA-8124041-1");
					pageTracker._setDomainName("bioethics.net");
					pageTracker._trackPageview();
				} catch(err) {}</script>
		<?php else: ?>
			<script type="text/javascript">
				var disqus_developer = 1; // developer mode is on
			</script>
		<?php endif; ?>
	</body>
</html>
