<?php require_once('header.php'); 

global $paged, $wp;
$has_post_type = false;
$posts_per_page = 20;
$vars['posts_per_page'] = 10000;
$vars['paged']			= $paged;
$qvars = get_query_var('post_type');
$type = (is_array($vars) && count($qvars)==3) ? $qvars[0] : false;
if($type):
	$vars['post_type']	= $type;
	$vars['posts_per_page'] = $posts_per_page;
	$has_post_type = true;
endif;

$new_query = array_merge( $vars, $wp->query_vars );
query_posts($new_query);
?>

<div id="main-content" class="hot-links">
<?php if ( have_posts() ) : 
	$topic = single_cat_title( '', false );
	$this_category = get_category($cat);
	?>
	<h2 class="rss">
		Hot Topics: <span> <?php
		$catstr = get_category_parents($cat, TRUE, ' &gt; ');
		echo substr($catstr, 0, strlen($catstr) -6 );?></span>
	</h2>
	<?php
	if (count(get_term_children($this_category->cat_ID,$this_category->taxonomy))>0) { ?>
	<div class="subcats">
		<h3>Subcategories</h3>
		<ul>
		<?php wp_list_categories('orderby=id&show_count=1&title_li=&use_desc_for_title=1&child_of='.$this_category->cat_ID); ?>
		</ul>
	</div>
	<?php } 

	get_template_part( 'loop','all' );
?>

<div id="pagination">
<?php if(is_single()): ?>
	<span class="nav-new">
		<?php next_post_link( '%link', 'Next <span class="meta-nav">&rarr;</span>'); ?>
	</span>
	<span class="nav-old">
		<?php previous_post_link( '%link','<span class="meta-nav">&larr;</span> Previous' ); ?>
	</span>
<?php else: ?>
	<span class="nav-old">
		<?php next_posts_link('&larr; Older entries '); ?>
	</span>
	<span class="nav-new">
		<?php previous_posts_link('Newer entries &rarr;'); ?>
	</span>
	<?php endif; ?>
</div><!-- /#pagination-->


	<?php else : ?>
	<h1>No Posts</h1>	
<?php endif; ?>
</div> <!-- /#main-content -->
<div id="sidebar-resources" class="sidebar section">
	<div id="hot-topics">
		<ul>
			<?php wp_list_categories('orderby=name&show_count=1&title_li=&title=&echo=false&exclude=210,29&feed_image='.$template_base.'/images/icon_rss_small.gif'); ?>
		</ul>
	</div>
</div>

<?php require_once('footer.php'); 