<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="s" class="assistive-text visuallyhidden">Search</label>
	<input type="text" class="field" name="s" id="s" value="<?=get_search_query()?>" placeholder="Search" />
	<input type="submit" class="submit button" name="submit" id="searchsubmit" value="Search" />
</form>
