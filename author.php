<?php require_once('header.php'); ?>

<div id="main-content" class="section">
<?php if ( have_posts() ) : ?>
    <h2 class="rss"><a href="<?php bloginfo('rss2_url'); ?>"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Blog RSS"></a> <a href="<?php bloginfo('url')?>/blog">Blog.</a></h2>
  <h2 class="rss">
      Author Archive: <?php echo '<a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( "ID" ) ) ) . '" title="' . esc_attr( get_the_author() ) . '" rel="me">' . get_the_author() . '</a>'; ?>
  </h2>
          <?php
        // If a user has filled out their description, show a bio on their entries.
        if ( get_the_author_meta( 'description' ) || get_the_author_meta( 'user_url' ) ) : ?>
        <div id="author-info">
          <div id="author-description">
            <h2>About <?php echo get_the_author(); ?></h2>
            <p id="author-bio"><?php the_author_meta( 'description' ); ?></p>
            <?php if ( get_the_author_meta( 'user_url' ) ) : ?>
              <p id="author-link"><a href="<?php the_author_meta( 'user_url' ); ?>" target="_blank">Website</a></p>
            <?php endif; ?>
          </div><!-- #author-description  -->
        </div><!-- #author-info -->
        <?php endif; ?>
  <?php
  get_template_part( 'loop' );

  else : ?>
  <h1>No Posts</h1> 

    <?php endif; ?>
</div> <!-- /#main-content -->
<div id="sidebar-blog" class="sidebar section">
  <ul>
    <?php dynamic_sidebar('blog-sidebar'); ?>
  </ul>
</div>

<?php require_once('footer.php'); 