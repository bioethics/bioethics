<?php require_once('header.php'); ?>
<div id="main-content" class="section">
  <h2 class="rss"><a href="<?php bloginfo('rss2_url'); ?>"><img src="<?= $template_base; ?>/images/btn-rss.png" width="20" height="21" alt="Blog RSS"></a> <a href="<?php bloginfo('url')?>/blog">Blog.</a></h2>
	<?php get_template_part('loop'); ?>
</div>
<?php
// if blog, show blog sidebar  
if (get_post_type() == 'post' && is_active_sidebar('blog-sidebar')):
	?>
	<div id="sidebar-blog" class="sidebar section">
		<ul>
			<?php dynamic_sidebar('blog-sidebar'); ?>
		</ul>
	</div>
	<?php
endif;
require_once('footer.php');