<?php 
/**
 * Template Name: About
 *
 */

require_once('header.php'); ?>
<h2 class="title"><?php the_title(); ?></h2>
<div id="page-content" class="section section about-content">
<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
			<div class="entry">
				<?php the_content(); ?>
				<?php edit_post_link('Edit this', ' <span class="edit-link">', '</span>'); ?>	
				<div class="social">
					<?php if( function_exists( do_sociable() ) ){ do_sociable(); } ?>
				</div>
			</div><!--end entry-->
		</div><!--end post-->
	<?php endwhile; ?>
</div>
<div id="sidebar-about" class="sidebar section">
	<?php
	wp_nav_menu(array(
		'theme_location' => 'about_nav',
		'container' => 'div',
		'container_id' => 'about-nav',
	));
	?>
</div>
<?php 
endif;
require_once('footer.php');