<?php
	global $posts_per_page, $has_post_type, $wp_query, $myposts;

	// separate results by type
	$i = 0;
	$x = 0;
	while ( have_posts() ) : the_post(); 
		$type = get_post_type();
		$myposts[$type][$i] = $post;
		if($type=='post' && $x < 10):
			$myposts[$type][$i]->excerpt = the_advanced_excerpt('length=20&exclude_tags=img',true);
			$x++;
		endif;
		$i++;
	endwhile; /* rewind or continue if all posts have been fetched */ 
	
	function post_count($type){
		global $has_post_type, $myposts, $wp_query;
		if(!$has_post_type):
			$count = count($myposts[$type]);
		else:
			$count = $wp_query->found_posts; 
		endif;
		return $count;
	}
	$i = 0;
	foreach($myposts as $key=>$value){
		$t = get_post_type_object($key);
		$t_count = post_count($key);
		$mytypes[$i]['plural'] = $t->labels->name; 
		$mytypes[$i]['key'] = $key; 
		$mytypes[$i]['count'] = $t_count;
		$i++;
	}
	function sortByOrder($a, $b) {
		return $a['count'] - $b['count'];
	}

	if(count($mytypes)>1):
		usort($mytypes, 'sortByOrder');
		$mytypes = array_reverse($mytypes);
		echo "<ul id=\"type_list\">";
		foreach($mytypes as $type):
			echo "<li><a href=\"#section_".$type['key']."\" title=\"".$type['plural']." : ".$type['count']." items \">".$type['plural']." </a></li>";
		endforeach;
		echo "</ul>";
	else:
		echo "<div class=\"totally_cheap_spacer\"></div>";
	endif;
	
	if($myposts['post']): ?>
	<div class="section" id="section_post">
		<h2 class="title divider">Blog Posts (<?= post_count('post'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['post'] as $p): ?>
		<div class="entry post">
			<h5 class="date"><?= date('F j, Y',strtotime($p->post_date)); ?></h5>
			<h4 class="title">
				<a href="<?= get_permalink($p->ID); ?>" title="<?= $p->post_title; ?>">
					<?= $p->post_title; ?>
				</a>
				<?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$p->ID); ?></h4>
			<?= $p->excerpt ?>
			
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['post'])>10 && $has_post_type==false) echo '<p class="more"><a class="more" href="'.$_SERVER['REQUEST_URI'].'/page/2/?post_type=post">View More Blog Entries</a></p>';
	?>
	</div>
	<?php
	endif;
	
	
	
	if(isset($myposts['articles'])): ?>
	<div class="section" id="section_articles">
		<h2 class="title divider">Published Articles (<?= post_count('articles'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['articles'] as $a):
			$article_fields = get_fields($a->ID); 
			$journal_id = $article_fields['ajob_issue']->ID;
			$journal_fields = get_fields($journal_id);
			?>
		<div class="entry article">
			<h5 class="date"><?php
				$tax = wp_get_post_terms($journal_id,'editions','name');
				echo $tax[0]->name;
			?>: Volume <?=$journal_fields['volume']; ?> Issue <?= $journal_fields['number']?> - <?php
			echo date('M Y',strtotime($journal_fields['publish_date']));
			?></h5>
			<h4 class="title"><a href="<?= get_permalink($a->ID) ?>" title="<?= $a->post_title ?>"><?= $a->post_title ?></a> <span><?= $article_fields['primary_author'] ?></span><?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$a->ID);?></h4>
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['articles'])>$posts_per_page-1 && $has_post_type==false) echo '<p class="more"><a class="more" href="?post_type=articles">View More Articles</a></p>';
	?>
	</div>
	<?php
	endif;
	
	
	
	if($myposts['links']): 
		$url = get_post_meta($post->ID, 'url', true);
    $soruce = get_post_meta($post->ID, 'source', true);
    $excerpt = get_post_meta($post->ID, 'excerpt', true);
	?>
	<div class="section" id="section_links">
		<h2 class="title divider">Resources (<?= post_count('links'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['links'] as $link): ?>
		<div class="entry link">
			<h4 class="title">
				<a href="<?= $url ?>" title="<?= $link->post_title ?>">
					<?= $link->post_title ?>
				</a>
				<?php if(isset($source)) echo '<span>'.$source.'</span>' ?>
				<?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$link->ID); ?>
			</h4>
			<?php if(isset($excerpt)):?>
				<p><?=$excerpt;?></p>
			<?php endif; ?>
			
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['links'])>10 && $has_post_type==false) echo '<p class="more"><a class="more" href="?post_type=links">View More Resources</a></p>';
	?>
	</div>
	<?php
	endif;
	
	
	
	
	if(isset($myposts['jobs'])): ?>
	<div class="section" id="section_jobs">
		<h2 class="title divider">Jobs (<?= post_count('jobs'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['jobs'] as $job):
			$fields = get_fields($job->ID); ?>
		<div class="entry jobs">
			<h5 class="date"><?= $fields['company_name'] ?></a><?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$job->ID); ?></h5>
			<h4 class="title"><a href="<?= get_permalink($job->ID) ?>" title="<?= $job->post_title ?>"><?= $job->post_title ?></a></h4>
		<p class="location">
		<?php
			$location = array();
			$city =	$fields['city'];
			if($city && $city !='') $location[] = $city;
			$state = $fields['state'];
			if($state && $state !='') $location[] = $state;
			$province = $fields['province'];
			if($province && $province!='') $location[] = $province;
			$country = $fields['country'];
			if($country && $country !='') $location[] = $country;

			echo implode(', ',$location);
		?>
		</p>
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['jobs'])>10 && $has_post_type==false) echo '<p class="more"><a class="more" href="?post_type=jobs">View More Jobs</a></p>';
	?>
	</div>
	<?php
	endif;
	
	
	
	
	
	if(isset($myposts['news'])): ?>
	<div class="section" id="section_news">
		<h2 class="title divider">News (<?= post_count('news'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['news'] as $n):
			$fields = get_fields($n->ID); ?>
		<div class="entry news">
			<h5 class="date"><?= date('F j, Y g:i a',strtotime($n->post_date)); ?></h5>
			<h4 class="title">
				<a href="<?= $fields['url'] ?>" title="<?= $n->post_title ?>">
					<?= $n->post_title ?>
				</a>
				<?php if($fields['source']) echo ' <span>('.$fields['source'].')</span>' ?>
				<?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$n->ID);?></h4>
			<?php if($fields['excerpt']):?>
				<?=$fields['excerpt'];?>
			<?php endif; ?>
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['news'])>10 && $has_post_type==false) echo '<p class="more"><a class="more" href="?post_type=news">View More News Items</a></p>';
	?>
	</div>
	<?php
	endif;
	
	
	
	if(isset($myposts['page'])): ?>
	<div class="section" id="section_page">
		<h2 class="title divider">Pages (<?= post_count('page'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['page'] as $p): ?>
		<div class="entry page">
			<h4 class="title">
				<a href="<?= get_permalink($p->ID); ?>" title="<?= $p->post_title ?>">
					<?= $p->post_title ?>
				</a>
				<?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$p->ID);?></h4>
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['page'])>10 && $has_post_type==false) echo '<p class="more"><a class="more" href="?post_type=page">View More Pages</a></p>';
	?>
	</div>
	<?php
	endif;
	
	
	
	if(isset($myposts['events'])): ?>
	<div class="section" id="section_events">
		<h2 class="title divider">Events (<?= post_count('events'); ?>)</h2>
		<?php
		$i = 0;
		foreach($myposts['events'] as $e):
			$fields = get_fields($e->ID); ?>
		<div class="entry event">
			<h5 class="date"><?= date('M d, Y',strtotime($e->post_date)); ?></h5>
			<h4 class="title"><a href="<?= get_permalink($e->ID) ?>" title="<?= $e->post_title ?>"><?= $e->post_title ?></a><?php echo edit_post_link('Edit','<span class="edit">(',')</span>',$e->ID); ?></h4>
			<p class="location">
			<?php
				$location = array();
				$city =	$fields['city'];
				if($city && $city !='') $location[] = $city;
				$state = $fields['state'];
				if($state && $state !='') $location[] = $state;
				$province = $fields['province'];
				if($province && $province!='') $location[] = $province;
				$country = $fields['country'];
				if($country && $country !='') $location[] = $country;

				echo implode(', ',$location);
			?>
			</p>
		</div>	
	<?php	$i++;
			if($i == 10) break;
		endforeach;
		if(count($myposts['events'])>10 && $has_post_type==false) echo '<p class="more"><a class="more" href="?post_type=events">View More Events</a></p>';
	?>
	</div>
	<?php
	endif;